package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pojo.Student;

import java.util.Arrays;
import java.util.HashMap;

@Controller
public class HelloControllerTest {
    @RequestMapping("/saveStudent")
    public String saveUser(Student student, String[]hobbies,
                           @RequestParam(name = "name",required = false,defaultValue ="tom" ) String studentname,
                           @RequestParam HashMap map){
        System.out.println("Student:"+student);
        System.out.println("class:"+student.getaClass());
        System.out.println(Arrays.toString(hobbies));
        System.out.println("map:"+map);
        System.out.println("studentname:"+studentname);
        System.out.println("conditions:"+student.getConditions());
        return "index.jsp";
    }
    @RequestMapping("/test/path")
    public String pathTest(){
        return "/studentRegister.jsp";
    }
}
