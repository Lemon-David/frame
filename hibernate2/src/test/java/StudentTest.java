import com.seecen.hibernate2.entity.HClass;
import com.seecen.hibernate2.entity.HStudent;
import com.seecen.hibernate2.entity.HStudentInfo;
import com.seecen.hibernate2.entity.HTeacher;
import com.seecen.hibernate2.util.DBUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class StudentTest {
    @Test
    public void curd(){
        Session session = DBUtil.getSession();
        Transaction transaction = session.beginTransaction();
//        HStudent hStudent = new HStudent();
//        hStudent.setName("杨过");
//        session.save(hStudent);
        HStudent hStudent = session.load(HStudent.class, 1L);
        System.out.println(hStudent);
        //修改
        hStudent.setName("张无忌");
        session.update(hStudent);
        //删除
        session.delete(hStudent);
        transaction.commit();
        DBUtil.close(session,DBUtil.sessionFactory);
    }
    @Test
    public void oneToOne(){
        Session session = DBUtil.getSession();
//        Transaction transaction = session.beginTransaction();
//        HStudent hStudent = new HStudent("乔治");
//        HStudentInfo hStudentInfo = new HStudentInfo(30, "加利福尼亚");
//        hStudent.setHstudentInfo(hStudentInfo);
//        hStudentInfo.setStudent(hStudent);
//        session.save(hStudent);
//        transaction.commit();
        HStudent hStudent = session.get(HStudent.class, 8L);
        System.out.println(hStudent);
        DBUtil.close(session, DBUtil.sessionFactory);
    }
    @Test
    public void oneToMany(){
        Session session = DBUtil.getSession();
//        Transaction transaction = session.beginTransaction();
//        HClass hClass = new HClass();
//        hClass.setName("大一");
//        session.save(hClass);
//        transaction.commit();
        HClass hClass = session.get(HClass.class, 1L);
        System.out.println(hClass);
        DBUtil.close(session,DBUtil.sessionFactory);
    }
    @Test
    public void manyToOne(){
        Session session = DBUtil.getSession();
        HStudent hStudent = session.get(HStudent.class, 1L);
        HStudent hStudent1 = session.get(HStudent.class, 3L);
        System.out.println(hStudent);
        System.out.println(hStudent1);
        Transaction transaction = session.beginTransaction();
        HStudent hStudent2 = new HStudent("维斯布鲁克");
        HStudentInfo hStudentInfo = new HStudentInfo(31, "加利福尼亚");
        HClass hClass = new HClass("大四");
        hStudent2.setHstudentInfo(hStudentInfo);
        hStudent2.setHclass(hClass);
        hStudentInfo.setStudent(hStudent2);
        session.save(hStudent2);
        transaction.commit();
        DBUtil.close(session,DBUtil.sessionFactory);
    }
    @Test
    public void manyToMany(){
        Session session = DBUtil.getSession();
        Transaction transaction = session.beginTransaction();
        HClass hClass = new HClass("高一");
        List<HTeacher> hTeachers = Arrays.asList(new HTeacher("乔丹"), new HTeacher("纳什"));
        hClass.setTeachers(hTeachers);
        session.save(hClass);
        transaction.commit();
        DBUtil.close(session,DBUtil.sessionFactory);
    }
    @Test
    public void manyToManyQuery(){
        HClass hClass = DBUtil.getSession().get(HClass.class, 6L);
        System.out.println(hClass);
        DBUtil.close(DBUtil.getSession(), DBUtil.sessionFactory);
    }
}
