import com.seecen.hibernate2.entity.HStudent;
import com.seecen.hibernate2.util.DBUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class HqlTest {
    @Test
    public void hqlTest(){
        Session session = DBUtil.getSession();
        //hql查询所有字段时，直接写from 类即可
        //from 类名（orm映射名）where 属性名=占位符（:名称==>?）
        String hql="from HStudent where name=:a or id=:id";
        //createQuery创建hql查询的对象 Query==>statement
        Query query = session.createQuery(hql);
        //setParameter为占位符赋值（占位符名，占位符对应值）
        query.setParameter("a","张三丰");
        query.setParameter("id",1L);
        //query.list 查询符合条件的所有记录，返回集合
        List<HStudent> list = query.list();
        for (HStudent hStudent:list) {
            System.out.println(hStudent);
        }
        DBUtil.close(session,DBUtil.sessionFactory);
    }
    /**
     * 当查询单个属性 select 属性名 from 类名 where 属性名
     * query.list()返回对应的属性类型的集合
     * 当查询多个属性时，select 属性名1，属性名2 ... from 类名 where 属性名...
     * query.list()返回Object[]类型的集合
     */
    @Test
    public void hqlTest2(){
        Session session = DBUtil.getSession();
        //hql查询所有字段时，直接写from 类即可
        //from 类名（orm映射名）where 属性名=占位符（:名称==>?）
        String hql="select name,id from HStudent where id=:id";
        //createQuery创建hql查询的对象 Query==>statement
        Query query = session.createQuery(hql);
        //setParameter为占位符赋值（占位符名，占位符对应值）
        query.setParameter("id",1L);
        //query.list 查询符合条件的所有记录，返回集合
        List<Object[]> list = query.list();
        for (Object[] name:list) {
//            System.out.println(name);
            System.out.println(Arrays.toString(name));
        }
        DBUtil.close(session,DBUtil.sessionFactory);
    }
    //分页查询，根据配置的方言，翻译成对应数据库的分页语句
    @Test
    public void hqlPageQuery(){
        Session session = DBUtil.getSession();
        Query query = session.createQuery("from HStudent order by id");
        //设置查询起始位置（0开始算）(pageNum-1)*pageSize
        query.setFirstResult(2);
        //设置要查询的记录条数pageSize=2
        query.setMaxResults(2);
        List<HStudent> list = query.list();
        for (HStudent hStudent:list) {
            System.out.println(hStudent);
        }
        DBUtil.close(session,DBUtil.sessionFactory);
    }
    //hibernate 执行原生sql查询
    @Test
    public void sqlQuery(){
        Session session = DBUtil.getSession();
        //通过createSQLQuery创建sql查询
        NativeQuery sqlQuery = session.createSQLQuery("select * from SC2001.H_STUDENT where NAME=?");
        //设置返回的数据对象类型
        sqlQuery.addEntity(HStudent.class);
        //给占位符赋值，下标从1开始
        sqlQuery.setParameter(1,"张无忌");
        //执行查询
        List<HStudent> list = sqlQuery.list();
        for (HStudent hStudent:list) {
            System.out.println(hStudent);
        }
        DBUtil.close(session, DBUtil.sessionFactory);
    }
    @Test
    public void criteriaQuery(){
        Session session = DBUtil.getSession();
        //创建Criteria查询
        Criteria criteria = session.createCriteria(HStudent.class);
        //from HStudent where name='张无忌'
        //.add 添加查询条件
        criteria.add(Restrictions.eq("name","张无忌"));
        List<HStudent> list = criteria.list();
        for (HStudent hStudent : list) {
            System.out.println(hStudent);
        }
        DBUtil.close(session,DBUtil.sessionFactory);
    }
    /**
     * 一级缓存 session级别缓存，通过同一个session进行查询时，session会将第一次查询的结果放入session缓存中
     * 后续的查询，会直接从缓存中获取。
     */
    @Test
    public void oneCache(){
        Session session = DBUtil.getSession();
        long start=System.currentTimeMillis();
        HStudent hStudent = session.get(HStudent.class, 1L);
        System.out.println("耗时："+(System.currentTimeMillis()-start));
        System.out.println(hStudent);
        System.out.println("==========第二次查询===========");
//        Session session1 = DBUtil.getSession();
//        HStudent hStudent2 = session1.get(HStudent.class, 1L);
        long start2=System.currentTimeMillis();
        HStudent hStudent2 = session.get(HStudent.class, 1L);
        System.out.println("耗时2："+(System.currentTimeMillis()-start2));
        System.out.println(hStudent2);
    }
    /**
     * 二级缓存：通常存放，查询频率高， 更新相对较少的数据
     */
    @Test
    public void secondCache(){
        Session session = DBUtil.getSession();
        HStudent hStudent = session.get(HStudent.class, 1L);
        System.out.println(hStudent);
        session.close();//关闭session
        System.out.println("======第二个session查询开始=========");
        Session session2 = DBUtil.getSession();
        HStudent hStudent1 = session2.get(HStudent.class, 1L);
        System.out.println(hStudent1);
        DBUtil.close(session2,DBUtil.sessionFactory);
    }
}
