import com.seecen.hibernate2.entity.HStudent;
import com.seecen.hibernate2.util.DBUtil;
import org.hibernate.LockOptions;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

public class LockTest {
    /**
     * 
     */
    @Test
    public void lockTest() {
        Session session = DBUtil.getSession();
        //开启事务
        Transaction transaction = session.beginTransaction();
        //指定LockOptions为UPGRADE
        //类似于数据库中指定 select * from h_student where id=1 for update;
        HStudent hStudent = session.get(HStudent.class, 1L, LockOptions.UPGRADE);
        System.out.println(hStudent);
        try {
            System.out.println("持有悲观锁，进入睡眠状态，模拟耗时操作");
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        transaction.commit();
        System.out.println("事务已提交释放锁："+System.currentTimeMillis());
        session.close();
    }
    @Test
    public void lockTest2() {
        Session session = DBUtil.getSession();
        Transaction transaction = session.beginTransaction();
        HStudent hStudent = session.get(HStudent.class, 1L);
        hStudent.setName("欧阳");
        session.update(hStudent);
        transaction.commit();
        System.out.println("数据修改成功："+System.currentTimeMillis());
        session.close();
    }
    /**
     * 乐观锁测试
     */
    @Test
    public void happyLock() {
        Session session = DBUtil.getSession();
        Transaction transaction = session.beginTransaction();
        HStudent hStudent = session.get(HStudent.class, 1L);
        System.out.println("事务提交前版本号："+hStudent.getVersion());
        hStudent.setName("黄药师");
        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        session.update(hStudent);
        transaction.commit();
        System.out.println("事务提交后版本号："+hStudent.getVersion());
        session.close();
    }
    @Test
    public void happyLock2() {
        Session session = DBUtil.getSession();
        Transaction transaction = session.beginTransaction();
        HStudent hStudent = session.get(HStudent.class, 1L);
        System.out.println("事务提交前版本号："+hStudent.getVersion());
        hStudent.setName("东邪");
        session.update(hStudent);
        transaction.commit();
        System.out.println("事务提交后版本号："+hStudent.getVersion());
        session.close();
    }
}
