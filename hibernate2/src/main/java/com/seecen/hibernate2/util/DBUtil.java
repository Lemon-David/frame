package com.seecen.hibernate2.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.Closeable;

public class DBUtil {
    public static SessionFactory sessionFactory;
    /*
     * 静态代码块 在类加载的时候自动执行
     * */
    static {
        //1. 加载配置文件
        Configuration configure = new Configuration().configure("/hibernate.cfg.xml");
        //2.创建SessionFactory
        sessionFactory = configure.buildSessionFactory();
    }

    public static Session getSession(){
        return sessionFactory.openSession();
    }
    /**
     * 关闭资源
     *  参数类型... 可变参数 类似于Closeable[]
     * @param closeables
     */
    public static void close(Closeable... closeables){
        if(closeables!=null){
            for (Closeable closeable : closeables) {
                try {
                    closeable.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
