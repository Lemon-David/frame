package com.seecen.hibernate2.entity;

import java.util.List;

public class HStudent {
    private long id;
    private String name;
    private HClass hclass;
    private HStudentInfo hstudentInfo;
    private List<HStudent> hstudents;
    //乐观锁版本号
    private Integer version;

    public HStudent() {
    }

    public HStudent(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HStudentInfo getHstudentInfo() {
        return hstudentInfo;
    }

    public void setHstudentInfo(HStudentInfo hstudentInfo) {
        this.hstudentInfo = hstudentInfo;
    }

    public HClass getHclass() {
        return hclass;
    }

    public void setHclass(HClass hclass) {
        this.hclass = hclass;
    }

    public List<HStudent> getHstudents() {
        return hstudents;
    }

    public void setHstudents(List<HStudent> hstudents) {
        this.hstudents = hstudents;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HStudent hStudent = (HStudent) o;

        if (id != hStudent.id) return false;
        if (name != null ? !name.equals(hStudent.name) : hStudent.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "HStudent{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", hclass=" + hclass +
                ", hstudentInfo=" + hstudentInfo +
                '}';
    }

}
