package com.seecen.hibernate2.entity;

import java.util.List;

public class HClass {
    private long id;
    private String name;
    private List<HStudent> students;
    private List<HTeacher> teachers;

    public HClass() {
    }

    public HClass(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<HStudent> getStudents() {
        return students;
    }

    public void setStudents(List<HStudent> students) {
        this.students = students;
    }

    public List<HTeacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<HTeacher> teachers) {
        this.teachers = teachers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HClass hClass = (HClass) o;

        if (id != hClass.id) return false;
        if (name != null ? !name.equals(hClass.name) : hClass.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "HClass{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", teachers=" + teachers +
                '}';
    }
}
