package com.seecen.hibernate2.entity;

public class HTeacher {
    private long id;
    private String name;

    public HTeacher() {
    }

    public HTeacher(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HTeacher hTeacher = (HTeacher) o;

        if (id != hTeacher.id) return false;
        if (name != null ? !name.equals(hTeacher.name) : hTeacher.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "HTeacher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
