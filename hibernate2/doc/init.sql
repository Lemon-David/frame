-- 创建表空间   datafiles 指定数据文件 size 指定默认大小 autoextend on 指定自动增长
-- DDL 自动提交事务
-- 通常一个项目 会创建一个用户，多个表空间 （日志表空间  索引表空间 数据表空间 。。）
create tablespace sc2001 datafile 'sc2001.dbf'  size 50M autoextend on;

--创建用户  identified by 密码  default tablespace  指定默认表空间
create user sc2001 identified by 123456 default tablespace  sc2001;

-- 赋予权限
-- 通常通过属于对应的角色 来赋予权限
-- connect 连接角色
-- resource 资源操作角色 表 索引 序列等对象操作
-- dba 数据管理员权限
grant connect,resource to sc2001;

--班级表
create table h_class(
    id number(11) primary key ,
    name varchar2(50) not null
) tablespace sc2001;
--学生表
create table h_student(
    id number(11) primary key ,
    name  varchar2(50) not null ,
    classid number(11) references h_class(id)
);
-- 学员详情
create table h_student_info(
    id number(11) primary key ,
    age number(3) not null ,
    address varchar2(100)
);
--老师表
create table h_teacher(
    id number(11) primary key ,
    name varchar2(50) not null
);
create table h_class_teacher(
    classid number(11) references h_class(id),
    teacherid number(11) references h_teacher(id)
);

create sequence seq_h_teacher;
create sequence seq_h_student;
create sequence seq_h_class;