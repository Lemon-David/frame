<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Lemon
  Date: 2020/6/2
  Time: 18:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>班级列表</title>
</head>
<body>
<form id="queryForm" action="${pageContext.request.contextPath}/class/list">
    班级名：<input type="text" name="name" value="${params.name}"><br>
    Test：<input type="text" name="test" value="${params.test}"><br>
    <input type="hidden" name="pageNum" value="1">
    <input type="hidden" name="pageSize" value="${page.pageSize}">
    <input type="submit" value="查询">
</form>
<a href="/class/toAdd">新增</a>
<table>
    <thead>
        <tr>
            <td>ID</td>
            <td>姓名</td>
            <td>test</td>
            <td>操作</td>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="aClass" items="${page.list}">
            <tr>
                <td>${aClass.id}</td>
                <td>${aClass.name}</td>
                <td>${aClass.test}</td>
                <td><a href="${pageContext.request.contextPath}/class/delete?id=${aClass.id}">删除</a></td>
                <td><a href="${pageContext.request.contextPath}/class/update?id=${aClass.id}">修改</a></td>
            </tr>
        </c:forEach>
    </tbody>
</table>
<div>
    <a href="javascript:void(0)" class="goPage" num="${page.pageNum-1}">上一页</a>
    <a href="javascript:void(0)" class="goPage" num="${page.pageNum+1}">下一页</a>
    <span href="">当前页：${page.pageNum}</span>
    <span href="">总页数：${page.pages}</span>
    <span href="">总记录数：${page.total}</span>
</div>
<script src="/static/js/jquery-3.2.1.min.js"></script>
<script>
    $(function ($) {
        $(".goPage").click(function () {
            var pageNum=$(this).attr("num");
            $("input[name='pageNum']").val(pageNum);
            $("#queryForm").submit();
        })
    })
</script>
</body>
</html>
