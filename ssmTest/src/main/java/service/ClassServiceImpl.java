package service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import entity.Class;
import mapper.ClassMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ClassServiceImpl implements ClassService {
    @Autowired
    private ClassMapper classMapper;
    @Override
    public PageInfo<Class> selectByPage(Map<String, Object> map) {
        Integer pageNum = map.get("pageNum") != null ?
                Integer.valueOf(map.get("pageNum").toString()) : 1;
        Integer pageSize = map.get("pageSize") != null ?
                Integer.valueOf(map.get("pageSize").toString()) : 2;
        PageHelper.startPage(pageNum,pageSize);
        List<Class> classes = classMapper.selectByMap(map);
        PageInfo<Class> pageInfo = new PageInfo<>(classes);
        return pageInfo;
    }

    @Override
    public Integer delete(Integer id) {
        int i=classMapper.delete(id);
        return i;
    }

    @Override
    public Class findById(Integer id) {
        return classMapper.findById(id);
    }

    @Override
    public int saveOrUpdate(Class aClass) {
        if (aClass.getId()==null){
            return classMapper.insert(aClass);
        }
        return classMapper.update(aClass);
    }
}
