package service;

import com.github.pagehelper.PageInfo;
import entity.Class;

import java.util.Map;


public interface ClassService {
    PageInfo<Class> selectByPage(Map<String,Object> map);
    Integer delete(Integer id);

    Class findById(Integer id);

    int saveOrUpdate(Class aClass);
}
