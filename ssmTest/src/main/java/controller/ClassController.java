package controller;

import com.github.pagehelper.PageInfo;
import entity.Class;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import service.ClassService;

import java.util.HashMap;

@Controller
@RequestMapping("/class")
public class ClassController {
    @Autowired
    private ClassService classService;
    @RequestMapping("/list")
    public String list(ModelMap modelMap, @RequestParam HashMap<String,Object> map){
        PageInfo<Class> page = classService.selectByPage(map);
        modelMap.put("page",page);
        modelMap.put("params",map);
        return "list.jsp";
    }

    @RequestMapping("/delete")
    public String delete (Integer id){
        classService.delete(id);
        return "redirect:list";
    }

    @RequestMapping("/update")
    public String update(Integer id,ModelMap modelMap){
        Class aClass=classService.findById(id);
        modelMap.put("aClass",aClass);
        return "update.jsp";
    }
    @RequestMapping("/save")
    public String saveOrUpdate(Class aClass){
        int i=classService.saveOrUpdate(aClass);
        return "redirect:list";
    }

    @RequestMapping("/toAdd")
    public String toAdd(){
        return "add.jsp";
    }
}
