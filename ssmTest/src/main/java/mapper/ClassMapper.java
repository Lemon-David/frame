package mapper;

import entity.Class;

import java.util.List;
import java.util.Map;

public interface ClassMapper {
    List<Class> selectByMap(Map<String,Object> map);

    int delete(Integer id);

    Class findById(Integer id);

    int insert(Class aClass);

    int update(Class aClass);
}
