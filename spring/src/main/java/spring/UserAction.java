package spring;

import before.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

/**
 * 通过spring IOC实现对象的创建和赋值
 */
// <bean class="spring.UserAction">
// 默认情况下Id值为类名首字母小写 可以通过(id)进行指定
@Controller("a")
//@Scope("prototype")//指定为多实例
@Lazy//延迟加载
public class UserAction {
    //自动注入注解
    @Qualifier("zhHelloService")
    //required 指定对象是否必须，为false则可以为null。默认为true
    @Autowired(required = false)
    private HelloService helloService;
    // Resource name 指定注入对象的名称（ID） 当一个接口有多个实现类的情况下
    // 如果没有通过name指定注入的对象名，则先会根据属性名称进行注入
    // 如果不存在对应的对象，则根据类型进行注入
    @Resource(name = "zhByeService")
    private ByeService zhByeService;

    public UserAction() {
        System.out.println("userAction对象创建成功！");
    }
    /**
     * 构造器注入，需要创建有参构造方法
     * @param helloService
     */
    public UserAction(HelloService helloService, ByeService byeService) {
        this.helloService = helloService;
        this.zhByeService = byeService;
    }
    @PostConstruct//配置初始化方法
    public void hello(){
        helloService.sayHello();
    }
    @PreDestroy//销毁方法
    public void bye(){
        zhByeService.bye();
    }
    public void setHelloService(HelloService helloService) {
        this.helloService = helloService;
    }
    public static void main(String[] args) {
//        UserAction userAction = new UserAction();
//        userAction.hello();
        //加载配置文件，创建ApplicationContext对象
        ApplicationContext applicationContext =
//                new ClassPathXmlApplicationContext("/applicationContext.xml");
                new ClassPathXmlApplicationContext("/applicationContext2.xml");
        //通过ApplicationContext对象获取bean（通过定义的ID）
//        UserAction userAction1 = (UserAction) applicationContext.getBean("userAction");
        UserAction userAction1 = (UserAction) applicationContext.getBean("a");
        UserAction userAction2 = (UserAction) applicationContext.getBean("a");
        System.out.println(userAction1==userAction2);//true 说明是同一个对象
        ClassPathXmlApplicationContext context = (ClassPathXmlApplicationContext) applicationContext;
        context.close();//关闭spring容器

//        userAction1.hello();
//        userAction1.bye();
    }

    /*public ByeService getByeService() {
        return byeService;
    }

    public void setByeService(ByeService byeService) {
        this.byeService = byeService;
    }*/

    /*public ByeService getZhByeService() {
        return zhByeService;
    }*/

    public void setZhByeService(ByeService byeService) {
        this.zhByeService = byeService;
    }
}
