package spring;

import org.springframework.stereotype.Service;

@Service("zhByeService")
public class ZhByeService implements ByeService{
    @Override
    public void bye() {
        System.out.println(" 再见 ");
    }
}
