package spring;

import org.springframework.stereotype.Service;

@Service("enByeService")//<bean class=''>
public class EnByeService implements ByeService {
    @Override
    public void bye() {
        System.out.println(" see you");
    }
}
