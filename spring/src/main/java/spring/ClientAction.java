package spring;

import before.HelloWang;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ClientAction {
    private HelloWang helloWang;

    public void sayHello(){
        helloWang.helloWang();
    }

    public HelloWang getHelloWang() {
        return helloWang;
    }

    public void setHelloWang(HelloWang helloWang) {
        this.helloWang = helloWang;
    }

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/applicationHelloWang.xml");
        ClientAction clientAction = (ClientAction) applicationContext.getBean("clientAction");
        clientAction.sayHello();
    }
}
