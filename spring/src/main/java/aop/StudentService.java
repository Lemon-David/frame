package aop;

public interface StudentService {
    int insert();
    int delete();
}
