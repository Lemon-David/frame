package aop;

import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements StudentService {
    @Override
    public int insert() {
        System.out.println("插入一条学生记录");
//        System.out.println("记录日志");//将操作记录保存数据库中
        return 0;
    }

    @Override
    public int delete() {
        System.out.println("删除一条学生记录");
//        System.out.println("记录日志");//将操作记录保存数据库中
        return 0;
    }
}
