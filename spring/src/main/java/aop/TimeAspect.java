package aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * 切面==>类
 */
@Component
@Aspect //切面注解
public class TimeAspect {
    /**
     * 定义一个空方法来申明切入点
     * 类似于在xml中定义<aop:pointcut></aop:pointcut>
     */
    @Pointcut("execution(* aop.StudentAction.*(..))")
    public void declarePointcut(){}
    /**
     * 通知方法==>方法==>切入的业务逻辑
     * @Before 指定为前置通知（声明的切入点方法）
     * @param joinPoint 连接点对象，可以获取连接点的一些基本信息
     */
    @Before("declarePointcut()")
    public void before(JoinPoint joinPoint){
        // 获取连接点方法的参数
        Object[] args = joinPoint.getArgs();
        System.out.println(Arrays.toString(args));
        // getSignature 获取连接点方法结构 getName 获取连接点方法名
        String name = joinPoint.getSignature().getName();
        System.out.println(name);
        // 获取目标类
        Object target = joinPoint.getTarget();
        System.out.println(target);
        System.out.println("开始执行"+target+"类的"+name+
                      "方法，参数列表为："+Arrays.toString(args));
        System.out.println("开始时间："+System.currentTimeMillis());
    }
    @After("declarePointcut()")
    public void after(JoinPoint joinPoint){
        System.out.println("结束时间："+System.currentTimeMillis());
    }
    /**
     * 环绕通知
     * @param joinPoint
     * @return
     */
    @Around("declarePointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("环绕通知前半部分处理");
        //执行连接点方法，并获取返回值
        Object result = joinPoint.proceed();
        System.out.println("环绕通知后半部分处理，获取到返回值："+result);
        //返回结果，此处可改变方法的返回结果
        return result;
    }
    /**
     * 异常通知
     * @param e 发生的异常
     */
    @AfterThrowing(value = "declarePointcut()",throwing = "e")
    public void afterThrowing(Exception e){
        System.out.println("进入异常通知");
        e.printStackTrace();
    }
    /**
     * 返回通知
     * @param result 连接点方法返回值
     */
    @AfterReturning(value = "declarePointcut()",returning = "result")
    public void afterReturning(Object result){
        System.out.println("返回通知，获取到方法返回值："+result);
    }
}
