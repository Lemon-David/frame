package aop;

import org.springframework.stereotype.Component;

/**
 * 日志切面
 */
@Component
public class LogAspect {
    /**
     * 通知
     */
    public void log(){
        System.out.println("切面统一记录日志！");
    }
}
