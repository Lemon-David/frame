package aop;

import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class timeAspectTest {
    public void timeBefore(){
        System.out.println("开始执行时间："+System.currentTimeMillis());
    }
    public void timeAfter(){
        System.out.println("结束执行时间："+System.currentTimeMillis());
    }
}
