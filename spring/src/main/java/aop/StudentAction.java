package aop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;

@Controller
public class StudentAction {
    @Autowired
    private StudentService studentService;

    public int insert(){
        int i=1 /0;
        return studentService.insert();
    }
    public int delete(Integer id,String name){
        return studentService.delete();
    }

    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("/applicationContext2.xml");
        StudentAction studentAction = (StudentAction) context.getBean("studentAction");
        studentAction.delete(1,"张三丰");
        studentAction.insert();
    }
}
