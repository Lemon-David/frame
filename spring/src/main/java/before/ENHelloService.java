package before;

import org.springframework.stereotype.Service;

//service 层注解，注解加在具体的实现类上，不是加在接口上
@Service("enHelloService")
public class ENHelloService implements HelloService {
    @Override
    public void sayHello() {
        System.out.println("hello, nice to meet you!");
    }
}
