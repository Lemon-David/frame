package before;

import org.springframework.stereotype.Service;

/**
 * 提供打招呼服务
 */
@Service("zhHelloService")
public class ZHHelloService implements HelloService{
    @Override
    public void sayHello() {
        System.out.println("你好！节日快乐");
    }
}
