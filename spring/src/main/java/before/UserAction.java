package before;

/**
 * 开发原则：
 *  开闭原则：对扩张开放，对修改关闭
 *
 *  面向接口编程：对象与对象之间的依赖通过接口来进行依赖
 *  工厂设计模式：工厂模式主要用来创建和提供对象。实现对象创建与对象使用的解耦合。
 */
public class UserAction {
    public void hello(){
        //在UserAction类中创建HelloService对象，并调用其方法
        //对象的创建交由工厂实现，此时UserAction与HelloService直接的依赖耦合关系便得以解除
        HelloService helloService = HelloServiceFactory.getHelloService();
        helloService.sayHello();
    }

    public static void main(String[] args) {
        UserAction userAction = new UserAction();
        userAction.hello();
    }
}
