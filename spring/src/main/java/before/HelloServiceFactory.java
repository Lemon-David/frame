package before;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 提供HelloService对象的工厂类
 */
public class HelloServiceFactory {
    public static HelloService getHelloService(){
        Properties properties = new Properties();
        /*将流对象放在try()中定义，会自动关闭流*/
        try(InputStream is =
                    HelloServiceFactory.class.getResourceAsStream("/helloService.properties");) {
            properties.load(is);//加载配置文件
            //获取配置文件内容 根据key取value值
            String helloServiceImpl = properties.getProperty("helloServiceImpl");
            //获取Class对象
            Class<HelloService> helloServiceClass = (Class<HelloService>) Class.forName(helloServiceImpl);
            //通过反射创建对象
            HelloService helloService = helloServiceClass.newInstance();
            return helloService;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
