import com.seecen.mybatis.mapper.StudentMapper;
import com.seecen.mybatis.pojo.Student;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class StudentTest {
    @Test
    public void test() throws IOException {
        //1.加载配置文件
        InputStream is = Resources.getResourceAsStream("mybatis.xml");
        //2.创建sqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        //3.通过SqlSessionFactory获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //4.通过SqlSession获取到Mapper接口
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        //5 通过mapper接口调用方法
        List<Student> students = mapper.selectAll();
        for (Student student : students) {
            System.out.println(student);
        }
        Student student = mapper.selectById(1);
        System.out.println(student);
        //6.关闭session
        sqlSession.close();
    }
}
