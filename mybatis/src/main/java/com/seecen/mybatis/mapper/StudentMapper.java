package com.seecen.mybatis.mapper;

import com.seecen.mybatis.pojo.Student;

import java.util.List;

/**
 * mybatis的dao层接口
 */
public interface StudentMapper {
    /**
     * 根据主键查询学生信息
     * @param id
     * @return
     */
    Student selectById(Integer id);

    /**
     * 查询所有的学生信息
     * @return
     */
    List<Student> selectAll();
}
