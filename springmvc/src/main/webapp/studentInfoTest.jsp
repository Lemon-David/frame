<%--
  Created by IntelliJ IDEA.
  User: Lemon
  Date: 2020/5/26
  Time: 19:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>学生信息</title>
</head>
<body>
<form id="queryForm">
    姓名：<input type="text" name="name"><br>
    年龄：<input type="text" name="age"><br>
    <input type="button" id="queryBtn" value="异步查询">
</form>
<table>
    <thead>
    <tr>
        <td>ID</td>
        <td>姓名</td>
        <td>性别</td>
        <td>年龄</td>
        <td>地址</td>
        <td>职位</td>
        <td>爱好</td>
        <td>生日</td>
    </tr>
    </thead>
    <tbody id="dataTable">
    </tbody>
</table>
<script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.min.js"></script>
<script>
    $(function($){
        $("#queryBtn").click(function(){
            var jsonObj={"name":$("input[name='name']").val(),"age":$("input[name='age']").val()};
            $.ajax({
                url:"${pageContext.request.contextPath}/studentTest/studentInfoTest",
                type:"post",
                data:JSON.stringify(jsonObj),
                dataType:"json",
                contentType:"application/json",
                success:function (result) {
                    alert("查询成功！");console.log(result);
                    var html="";
                    for (var i = 0; i < result.length; i++) {
                        html+="<tr>\n" +
                        "            <td>"+result[i].id+"</td>\n" +
                        "            <td>"+result[i].name+"</td>\n" +
                        "            <td>"+result[i].sex+"</td>\n" +
                        "            <td>"+result[i].age+"</td>\n" +
                        "            <td>"+result[i].address+"</td>\n" +
                        "            <td>"+result[i].position.name+"</td>\n" +
                        "            <td>"+result[i].hobbies+"</td>\n" +
                        "            <td>"+result[i].birthday+"</td>\n" +
                        "        </tr>"
                    }
                    console.log(html);
                    $("#dataTable").html(html);
                }
            })
        })
    })
</script>
</body>
</html>
