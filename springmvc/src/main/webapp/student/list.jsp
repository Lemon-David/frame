<%--
  Created by IntelliJ IDEA.
  User: Lemon
  Date: 2020/5/27
  Time: 10:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<html>
<head>
    <title>学生列表页面</title>
</head>
<body>
<form id="queryForm">
    姓名：<input type="text" name="name"><br>
    年龄：<input type="text" name="age"><br>
    <input type="button" value="异步查询" id="queryBtn">
</form>
<table>
    <thead>
        <tr>
            <td>ID</td>
            <td>姓名</td>
            <td>年龄</td>
            <td>生日</td>
        </tr>
    </thead>
    <tbody id="dataTabel">
    </tbody>
</table>
<%--
    js 404 1）路径写错 2）没有编译到==>查看target目录 maven clean
           3）springmvc 没有配置default-servlet-handler
           4）拦截器拦截
--%>
<script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.min.js"></script>
<script>
    $(function ($) {
        $("#queryBtn").click(function(){
            console.log($("#queryForm").serialize());
            console.log(JSON.stringify($("#queryForm").serialize()));
            var obj={"name":$("input[name='name']").val(),"age":$("input[name='age']").val()};
            $.ajax({
                url:"${pageContext.request.contextPath}/student/list",
                type:"post",
                data:JSON.stringify(obj),//$("#queryForm").serialize()==>name=xxx&age=xxx,contentType没有指定为Json时使用
                dataType:"json",
                contentType:"application/json",//以json字符串的方法传递数据
                success:function(result){
                    alert("查询成功");console.log(result);
                    var html="";
                    for (var i = 0; i < result.length; i++) {
                        html+="<tr>\n" +
                            "            <td>"+result[i].id+"</td>\n" +
                            "            <td>"+result[i].name+"</td>\n" +
                            "            <td>"+result[i].age+"</td>\n" +
                            "            <td>"+result[i].birthday+"</td>\n" +
                            "        </tr>"
                    }
                    console.log(html);
                    $("#dataTabel").html(html);//替换tbody标签的html内容
                }
            })
        })
    })
</script>
</body>
</html>
