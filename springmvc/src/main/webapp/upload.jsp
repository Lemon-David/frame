<%--
  Created by IntelliJ IDEA.
  User: Lemon
  Date: 2020/5/28
  Time: 14:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>文件上传</title>
</head>
<body>
<%--
  1）文件上传，必须指定为post方式提交表单
  2）enctype指定为multipart/form-data
--%>
<form action="${pageContext.request.contextPath}/fileUpload"
      method="post" enctype="multipart/form-data">
    选择上传文件：<input type="file" name="headPic">
    <input type="submit" value="上传">
</form>
</body>
</html>
