package interceptor;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import pojo.Role;
import pojo.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 权限拦截器
 */
public class AuthorityInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //获取到session域中的用户信息
        User user = (User) request.getSession().getAttribute("user");
        //获取角色信息
        Role role = user.getRole();
        if (role != null){
            //获取当前的请求的Controller
            String controllerName="";
            if (handler instanceof HandlerMethod){
                HandlerMethod method= (HandlerMethod) handler;
                controllerName = method.getBeanType().getSimpleName();
            }
            Map<String, List<String>> map = new HashMap<>();
            //  角色名   角色拥有的权限
            map.put("admin", Arrays.asList("StudentController","IndexController"));
            map.put("test",Arrays.asList("UserController"));
            //判断角色与请求的控制层的关系
            List<String> list = map.get(role.getName());
            if (list!=null && list.contains(controllerName)){
                return true;
            }
        }
        response.sendRedirect(request.getContextPath()+"/403.jsp");
        return false;
    }
}
