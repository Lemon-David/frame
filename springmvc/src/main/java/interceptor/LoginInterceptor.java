package interceptor;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import pojo.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * springmvc 控制层拦截器实现
 */
public class LoginInterceptor implements HandlerInterceptor {
    /**
     * 请求到达控制层之前执行
     * @param request
     * @param response
     * @param handler 请求的对象，如果是控制层请求，则为HandleMethod对象，可通过该对象获取控制层方法信息
     * @return 返回true，则放行，返回false则拦截
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("进入拦截器");
        //判定handler是否为控制层方法
        if (handler instanceof HandlerMethod){
            HandlerMethod handlerMethod=(HandlerMethod)handler;
            //获取方法名
            String name = handlerMethod.getMethod().getName();
            System.out.println("方法名："+name);
            Class<?> beanType = handlerMethod.getBeanType();
            System.out.println("控制层类名："+beanType.getName());
        }
        //去session中获取登录标识
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user!=null){//用户已登录，放行
            return true;
        }
        //如果没有登录，则跳转到登录页面
        response.sendRedirect(request.getContextPath()+"/login.jsp");
        return false;
    }

    /**
     * 控制层方法调用之后，视图解析器解析之前执行
     * @param request
     * @param response
     * @param handler
     * @param modelAndView 控制层方法返回的模型视图对象
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("进入拦截器postHandler方法");
        if (modelAndView!=null){
            System.out.println("需要跳转的页面："+modelAndView.getViewName());
            System.out.println("返回的Model数据："+modelAndView.getModel());
            System.out.println("返回的ModelMap数据："+modelAndView.getModelMap());
            modelAndView.addObject("postHandleInfo","postHandle中添加的值");
        }
    }

    /**
     * 在视图解析之后执行，通常做一些清理工作
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("进入afterCompletion方法");
    }
}
