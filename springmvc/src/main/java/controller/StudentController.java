package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pojo.Student;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/student")
public class StudentController {
    /**
     * @RequestBody 接收json字符串数据，并封装到对象中
     * @param student
     * @return
     */
    @ResponseBody//返回json数据
    @RequestMapping("/list")
    public List<Student> list(@RequestBody Student student){
        System.out.println("获取到数据："+student);
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student(1,"路飞",19,new Date()));
        students.add(new Student(2,"乔巴",9,new Date()));
        students.add(new Student(3,"娜美",19,new Date()));
        return students;
    }
    @RequestMapping("/detail")
    public String detail(){
        return "/index.jsp";
    }
}
