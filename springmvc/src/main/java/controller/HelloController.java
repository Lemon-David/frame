package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
    /**
     * springmvc的处理方法
     * RequestMapping 指定方法对应的请求地址
     * return 页面地址 表示方法执行完成之后跳转到对应的页面（转发）
     * @return
     */
    @RequestMapping("/hello")
    public String hello(){
        System.out.println("进入hello方法");
        return "index.jsp";
    }
}

