package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pojo.Position;
import pojo.StudentTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/studentTest")
public class StudentControllerTest {
    @ResponseBody
    @RequestMapping("/studentInfoTest")
    public List<StudentTest> list(@RequestBody(required = false) StudentTest studentTest){
        System.out.println("获取到异步请求数据："+ studentTest);
        ArrayList<StudentTest> studentTests = new ArrayList<>();
        studentTests.add(new StudentTest(1,"詹姆斯","男",36,"洛杉矶",
                new Position("班长"), Arrays.asList("打篮球","玩游戏"),new Date()));
        return studentTests;
    }
}
