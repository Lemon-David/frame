package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import pojo.Role;
import pojo.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
@RequestMapping("/user")//指定全局的请求前缀
public class UserController {
    /**
     * 用户详情
     * 演示向前台传值
     * 1）在方法参数中定义Map，Model，ModelMap，并通过这三个对象向前台传递值
     * ==>request.setAttribute(key,value)
     * @return
     */
    @RequestMapping("/detail")
    public String detail(Integer id,
//                         ModelMap modelMap
//                         Map modelMap
                         Model model
    ){//404 资源找不到，1）请求路径 2）后台资源不存在（controller，页面）
        HashMap<String, String> conditions = new HashMap<>();
        conditions.put("sal","11111111");
        conditions.put("age","22222222");
        //todo 去数据库查询用户信息
        System.out.println("查询Id为"+id+"的用户记录");
        User user = new User(id, "詹姆斯", 18, "男", "江西南昌",
                new Role(100, "java工程师"),
                Arrays.asList("打篮球", "rap"),conditions);
        //通过modelMap向前台传值==>request.setAttribute(key,value)
//        modelMap.put("user",user);
        //通过Model对象传递数据
        model.addAttribute("user",user);
        return "detail.jsp";
    }

    /**
     * 演示通过ModelAndView向页面传值和跳转页面
     *       @ModelAttribute 注解
     *                  1）加在方法参数上，将对象传递到request域中，或向request域中取值
     *                  2）加在方法上，将方法的返回值放到request域中
     * @param id
     * @return
     */
    @RequestMapping("/detail2")
    public ModelAndView detail2(Integer id, @ModelAttribute("username") String username,
                                HttpServletRequest request,
                                HttpSession session,
                                HttpServletResponse response
                                ){//404 资源找不到，1）请求路径 2）后台资源不存在（controller，页面）
        request.setAttribute("requestTest","请求域数据");
        session.setAttribute("sessionTest","session域数据");
        HashMap<String, String> conditions = new HashMap<>();
        conditions.put("sal","11111111");
        conditions.put("age","22222222");
        //todo 去数据库查询用户信息
        System.out.println("查询Id为"+id+"的用户记录");
        User user = new User(id, "詹姆斯", 18, "男", "江西南昌",
                new Role(100,"java软件工程师"),
                Arrays.asList("打篮球","rap"),conditions);
        //通过ModelAndView 设置跳转的页面和值
        ModelAndView modelAndView = new ModelAndView();
        //向页面传值
        modelAndView.addObject("user",user);
        //指定跳转的页面 以/开头，则直接到资源根目录下找webapp
        //            不以/开头，跟在RequestMapping最后一个/后面
        modelAndView.setViewName("detail.jsp");
        return modelAndView;
    }

    /**
     * 重定向演示
     * @return
     */
    @RequestMapping("/redirectTest")
    public String redirectTest(){
        return "redirect:/user/detail2";
    }
    /**
     * 将方法返回值放入request域中
     * @return
     */
    @ModelAttribute(name = "modelAttributeTest")
    public String test(){
        return "我是ModelAttribute测试";
    }

    @ResponseBody//指定返回json数据，不跳转页面
    @RequestMapping("/list")
    public List<User> list(@RequestBody(required = false) User user){
        System.out.println("获取到异步请求数据："+user);
        //todo 根据条件做数据库查询，返回结果集合
        ArrayList<User> users = new ArrayList<>();
        users.add(new User(1,"james",18,"男"));
        users.add(new User(2,"姚明",19,"男"));
        users.add(new User(3,"路飞",17,"男"));
        return users;
    }

}
