package controller;

import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Controller
public class FileController {
    /**
     * MultipartFile 用来j接收前台传递的文件对象
     * @return
     */
    @RequestMapping("/fileUpload")
    public String upload(MultipartFile headPic) throws IOException {
        //获取到上传的文件名
        String filename = headPic.getOriginalFilename();
        System.out.println("文件名："+filename);//a.jpg
        //设定保存路径
        String savePath="D:/Java工程师(全栈式)(王的文件)/Struts2、hibernate、mybatis、spring、springmvc/05-28/upload/";//
        //创建文件对象
        File file = new File(savePath + filename);
        if (!file.getParentFile().exists()){
            file.getParentFile().mkdirs();//如果父目录不存在创建目录
        }
        //将上传的文件保存到指定的文件对象中
        headPic.transferTo(file);
        return "download.jsp";
    }
    @RequestMapping("/fileDownload")
    public ResponseEntity<byte[]> download(String fileName) throws IOException {
        //设定保存的路径
        String savePath="D:/Java工程师(全栈式)(王的文件)/Struts2、hibernate、mybatis、spring、springmvc/05-28/upload/";
        File file = new File(savePath + fileName);
        if (!file.exists()){//如果文件不存在
            return null;
        }
        //构建响应头
        HttpHeaders headers = new HttpHeaders();
        //设置以附件形式打开，设定附件文件名
        headers.setContentDispositionFormData("attachment",fileName);
//        List<MediaType> accept = headers.getAccept();
//        for (MediaType mediaType : accept) {
            //设置ContentType为Stream
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
//        }
        byte[] bytes = FileUtils.readFileToByteArray(file);
        return new ResponseEntity<byte[]>(
                bytes,//文件的字节数组
                headers,
                HttpStatus.CREATED);
    }
}
