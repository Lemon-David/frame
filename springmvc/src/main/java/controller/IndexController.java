package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import pojo.Role;
import pojo.User;

import javax.servlet.http.HttpSession;

@Controller
public class IndexController {
    /**
     * 作业：
     * 1）如果登录的用户是admin，拥有管理员角色，可以访问StudentController
     * 2）如果登录的用户是test，拥有测试管理员角色，可以访问UserController的方法
     * 3）如果没有权限，则跳转到没有权限的页面
     * @param name
     * @param password
     * @param session
     * @return
     */
    @RequestMapping("/login")
    public String login(String name, String password,HttpSession session){
        System.out.println("获取到前台数据：name="+name+"，password="+password);
        //todo 查询数据库，验证是否存在用户数据
        User user = new User();
        user.setName(name);
        Role role = null;
        if ("admin".equals(name)){
             role = new Role(1, "admin");
        }else if ("test".equals(name)){
             role = new Role(2, "test");
        }
        user.setRole(role);
        if (user==null){//如果用户不存在，跳回登录页面
            return "login.jsp";
        }
        //登录成功，将用户信息放入session
        session.setAttribute("user",user);
        return "index.jsp";
    }
    @RequestMapping("/home")
    public String home(){
        return "index";
    }
}
