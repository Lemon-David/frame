package pojo;

import java.util.List;
import java.util.Map;

public class User {
    private Integer id;
    private String name;
    private Integer age;
    private String sex;
    private String addr;
    //用来接收前台的数据
    private Role role;
    // 用来接收前台的多个同名数据
//    private String[] hobbies;
    private List<String> hobbies;
    // 通过map接收前台的值
    private Map<String,String> conditions;

    public User() {
    }

    public User(Integer id, String name, Integer age, String sex) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public User(Integer id, String name, Integer age, String sex, String addr, Role role, List<String> hobbies, Map<String, String> conditions) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.addr = addr;
        this.role = role;
        this.hobbies = hobbies;
        this.conditions = conditions;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", addr='" + addr + '\'' +
                ", role=" + role +
                ", hobbies=" + hobbies +
                ", conditions=" + conditions +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<String> getHobbies() {
        return hobbies;
    }

    public void setHobbies(List<String> hobbies) {
        this.hobbies = hobbies;
    }

    public Map<String, String> getConditions() {
        return conditions;
    }

    public void setConditions(Map<String, String> conditions) {
        this.conditions = conditions;
    }
}
