<%--
  Created by IntelliJ IDEA.
  User: Lemon
  Date: 2020/5/26
  Time: 14:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>用户列表页面</title>
</head>
<body>
    <input type="button" id="btn" value="异步加载数据">
    <script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.min.js"></script>
    <script>
        $(function($){
            $("#btn").click(function(){
                var jsonObj={"name":"james","age":"18"};
                $.ajax({
                    url:"${pageContext.request.contextPath}/user/list",
                    type:"post",
                    data:JSON.stringify(jsonObj),//json字符串
                    dataTyp:"json",
                    contentType:"application/json",//指定以json方式传输数据
                    success:function(result){
                        alert("请求成功！")
                        console.log(result)
                        //到页面展示
                    }
                })
            })
        })
    </script>
</body>
</html>
