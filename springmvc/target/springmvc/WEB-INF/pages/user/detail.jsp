<%--
  Created by IntelliJ IDEA.
  User: Lemon
  Date: 2020/5/26
  Time: 10:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>用户详情页面</title>
</head>
<body>
  <table>
      <tr>
          <td>用户名</td>
          <td>${user.name}</td>
          <td>年龄</td>
          <td>${user.age}</td>
      </tr>
      <tr>
          <td>性别</td>
          <td>${user.sex}</td>
          <td>地址</td>
          <td>${user.addr}</td>
      </tr>
  </table>

获取@ModelAttribute设置的值：${username}<br>
获取@ModelAttribute设置的值2：${modelAttributeTest}<br>
获取Request设置的值：${requestTest}<br>
获取Session设置的值：${sessionTest}<br>
获取PostHandler中设置的值：${postHandleInfo}<br>
</body>
</html>
