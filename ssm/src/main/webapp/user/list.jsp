<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Lemon
  Date: 2020/5/29
  Time: 17:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>用户列表</title>
</head>
<body>
<form id="queryForm" action="${pageContext.request.contextPath}/user/list">
    姓名；<input type="text" name="name" value="${params.name}"><br>
    班级；<input type="text" name="classid" value="${params.classid}"><br>
    <input type="hidden" name="pageNum" value="1">
    <input type="hidden" name="pageSize" value="${page.pageSize}">
    <input type="submit" value="查询">
</form>
<a href="/user/toAdd">新增</a>
<table>
    <thead>
        <tr>
            <td>ID</td>
            <td>姓名</td>
            <td>班级</td>
            <td>version</td>
            <td>操作</td>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="student" items="${page.list}">
            <tr>
                <td>${student.id}</td>
                <td>${student.name}</td>
                <td>${student.classid}</td>
                <td>${student.version}</td>
                <td><a href="${pageContext.request.contextPath}/user/delete?id=${student.id}">删除</a></td>
                <td><a href="${pageContext.request.contextPath}/user/update?id=${student.id}">修改</a></td>
            </tr>
        </c:forEach>
    </tbody>
</table>
<div>
    <a href="javascript:void(0)" class="goPage" num="${page.pageNum-1}">上一页</a>
    <a href="javascript:void(0)" class="goPage" num="${page.pageNum+1}">下一页</a>
    <span href="">当前页：${page.pageNum}</span>
    <span href="">总页数：${page.pages}</span>
    <span href="">总记录数：${page.total}</span>
</div>
<script src="/static/js/jquery-3.2.1.min.js"></script>
<script>
    $(function ($) {
        $(".goPage").click(function () {
            var pageNum=$(this).attr("num");//获取标签属性值
            $("input[name='pageNum']").val(pageNum);
            //提交表单
            $("#queryForm").submit();
        })
    })
</script>
<%--<form id="queryForm" method="post">
    请输入你要查询的学生姓名：<input type="text" name="name">
    <input type="button" value="查询" id="queryBtn">
</form>
<table>
    <thead>
        <tr>
            <td>ID</td>
            <td>姓名</td>
            <td>班级</td>
            <td>version</td>
        </tr>
    </thead>
    <tbody id="dataTable"></tbody>
    <script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.min.js"></script>
    <script>
        $(function ($) {
            $("#queryBtn").click(function(){
                console.log($("#queryForm").serialize());
                console.log(JSON.stringify($("#queryForm").serialize()));
                var obj={"name":$("input[name='name']").val()};
                $.ajax({
                    url:"/user/select",
                    type:"post",
                    data:JSON.stringify(obj),
                    dataType:"json",
                    contentType:"application/json",
                    success:function(result){
                        alert("查询成功");console.log(result);
                        var html="";
                        for (var i = 0; i < result.length; i++) {
                            html+="<tr>\n" +
                                "            <td>"+result[i].id+"</td>\n" +
                                "            <td>"+result[i].name+"</td>\n" +
                                "            <td>"+result[i].classid+"</td>\n" +
                                "            <td>"+result[i].version+"</td>\n" +
                                "        </tr>"
                        }
                        console.log(html);
                        $("#dataTable").html(html);
                    }
                })
            })
        })
    </script>
</table>--%>
</body>
</html>
