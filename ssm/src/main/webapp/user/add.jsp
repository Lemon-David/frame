<%--
  Created by IntelliJ IDEA.
  User: Lemon
  Date: 2020/6/2
  Time: 10:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学生信息新增</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/user/save" method="post">
    姓名：<input type="text" name="name"><br>
    班级id：<input type="text" name="classid"><br>
    版本：<input type="text" name="version"><br>
    <%--将数据封装到student对象的studentInfo对象中--%>
    年龄：<input type="text" name="studentInfo.age"><br>
    地址：<input type="text" name="studentInfo.address"><br>
    <input type="submit" value="保存">
</form>
</body>
</html>
