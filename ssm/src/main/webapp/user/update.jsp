<%--
  Created by IntelliJ IDEA.
  User: Lemon
  Date: 2020/6/1
  Time: 19:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>修改页面</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/user/save" method="post">
    <%--通过隐藏域回传id值--%>
    <input type="hidden" name="id" value="${student.id}">
    姓名：<input type="text" name="name" value="${student.name}"><br>
    班级id：<input type="text" name="classid" value="${student.classid}"><br>
    版本：<input type="text" name="version" value="${student.version}"><br>
    <input type="submit" value="保存">
</form>
</body>
</html>
