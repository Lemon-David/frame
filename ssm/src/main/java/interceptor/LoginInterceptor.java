package interceptor;

import entity.User;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("进入拦截器");
        if (handler instanceof HandlerMethod){
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            String name = handlerMethod.getMethod().getName();
            System.out.println("方法名"+name);
            Class<?> beanType = handlerMethod.getBeanType();
            System.out.println("控制层类名："+beanType.getName());
        }
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user!=null){
            return true;
        }
        response.sendRedirect(request.getContextPath()+"/user/login.jsp");
        return false;
    }
}
