package aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义注解
 */
@Target({ElementType.METHOD,ElementType.FIELD})//指定作用的目标对象（可以添加的位置）
@Retention(RetentionPolicy.RUNTIME)
public @interface LogData {
    //定义注解中的属性
    String description() default "";
    //日志类型 1 登录 2 删除 3 修改
    LogType logType();
}
