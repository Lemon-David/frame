package aop;

/**
 * 日志类型枚举
 * 枚举是一个特殊的类
 * class 可以创建n个对象
 * 枚举类型的对象是固定的
 */
public enum LogType {
    //创建枚举对象，对象的个数是有限的，对象与对象之间用逗号隔开
    LOGIN(1),DELETE(2),UPDATE(3),INSERT(4);
    //可以定义任意的方法和属性，与普通类类似
    private final int type;
    //构造方法
    LogType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

}
