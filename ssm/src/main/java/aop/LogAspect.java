package aop;

import entity.Student;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import service.StudentService;
import sun.misc.Request;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Map;

@Component//对象由spring管理
@Aspect//切面注解
public class LogAspect {
    @Autowired
    private StudentService studentService;
    //定义切入点，切入到添加了LogData注解的方法上
    @Pointcut("@annotation(aop.LogData)")
    public void pointCut(){

    }
    /**
     * 记录日志的切面方法
     * 在该方法中定义统一的日志记录逻辑
     * @param joinPoint
     */
    public static final Logger LOGGER= LogManager.getLogger(LogAspect.class);
    @Before("pointCut()")
    public void log(JoinPoint joinPoint){
        System.out.println("进入日志Aspect");
        ///获取到方法签名
        MethodSignature signature=(MethodSignature)joinPoint.getSignature();
        //获取到连接点方法对象
        Method method = signature.getMethod();
        //获取方法上面特定的注解
        LogData annotation = method.getAnnotation(LogData.class);
        LogType logType=annotation.logType();
        String description = annotation.description();
        LOGGER.info("获取到注解内容：logType"+logType.getType()+
                ",description:"+description);
        //aop中获取request
        ServletRequestAttributes requestAttributes=
                (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request=requestAttributes.getRequest();
        HttpSession session =request.getSession();
        //获取操作人
        Student student=(Student)session.getAttribute("student");
        //获取请求数据
        Map<String, String[]> parameterMap = request.getParameterMap();
        //将对象转换成json字符串==>存储到请求数据字段中
        //jackSon json
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String s = objectMapper.writeValueAsString(parameterMap);
            LOGGER.info("请求数据"+s);
//            studentService.insertAll(student,description,s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //

}
