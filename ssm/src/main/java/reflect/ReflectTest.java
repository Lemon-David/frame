package reflect;

import aop.LogData;
import entity.Student;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class ReflectTest {
    public static void main2(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException {
        //获取class对象，一个类只有一个Class对象
        Class<Student> studentClass = Student.class;
//        Class<? extends Student> aClass = new Student().getClass();
        Class<?> aClass1 = Class.forName("entity.Student");
//        System.out.println(studentClass==aClass);
        System.out.println(studentClass==aClass1);
        //通过反射创建对象
        //1.newInstance创建对象，调用无参构造
//        Student student = studentClass.newInstance();
        //2.通过构造器创建对象
        //获取声明的无参构造器d
        Constructor<Student> constructor = studentClass.getDeclaredConstructor();
        //设置是否允许访问，当private方法设置为true后，可以被反射直接调用
        constructor.setAccessible(true);
        //通过构造器创建对象
        Student student = constructor.newInstance();
        Student student2 = constructor.newInstance();
        System.out.println(student==student2);//false
        getClassInfo();
    }

    /**
     * 演示获取类的各种信息
     */
    public static void getClassInfo() throws NoSuchFieldException {
        Class<Student> studentClass = Student.class;
        //获取类上的注解
        Annotation[] annotations = studentClass.getAnnotations();
        //根据名称获取单个属性
        Field name = studentClass.getDeclaredField("name");
        name.setAccessible(true);
        LogData annotation = name.getAnnotation(LogData.class);
        Field[] fields = studentClass.getFields();//获取类中的public属性
        Field[] declaredFields = studentClass.getDeclaredFields();//获取类中定义的所有属性
        //获取类的方法
        Method[] methods = studentClass.getMethods();//获取类中public的方法（包含父类方法）
        Method[] methods2 = studentClass.getDeclaredMethods();//获取当前类中所有方法
        //获取实现的接口
        Class<?>[] interfaces = studentClass.getInterfaces();
        //获取类的父类
        Class<? super Student> superclass = studentClass.getSuperclass();
        //包含包名的类名
        String name1 = studentClass.getName();
        //不包含包名的类名
        String name3 = studentClass.getSimpleName();

    }
    /**
     * 反射对对象的操作
     */
    public static void objectOperations(Class clazz) throws IllegalAccessException, InstantiationException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException {
        //创建对象
        Object o = clazz.newInstance();
        //给对象属性赋值
        //getDeclaredField
        Field name = clazz.getDeclaredField("name");
        //设置访问权限
        name.setAccessible(true);
        //给属性赋值
        name.set(o,"姚明");
        System.out.println(o);
//        System.out.println(name.getName());//属性名
        //调用对象的方法
        //获取到对应方法
        Method setClassid = clazz.getDeclaredMethod("setClassid", Integer.class);
        Object invoke = setClassid.invoke(o, 100);
        System.out.println(o);
    }

    public static void main(String[] args) throws InstantiationException, IllegalAccessException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException {
        objectOperations(Student.class);
        // /save
        HashMap<String, Object> map = new HashMap<>();
        map.put("name","james");
        map.put("classid",100);
        map.put("version",2);
        //将map数据封装到 save方法中的参数中去，并调用save方法
        Class<ReflectTest> reflectTestClass = ReflectTest.class;
        ReflectTest reflectTest = reflectTestClass.newInstance();
        //获取当前类的方法
        Method[] declaredMethods = reflectTestClass.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            //判断方法是否有添加RequestMapping注解
            RequestMapping annotation = declaredMethod.getAnnotation(RequestMapping.class);
            if (annotation!=null){
                if ("/save".equals(annotation.value()[0])){
                    //收到方法的参数列表类型
                    Class<?>[] parameterTypes = declaredMethod.getParameterTypes();
                    if (parameterTypes!=null&&parameterTypes.length>0){
                        //创建对象
                        Class<?> parameterClazz = parameterTypes[0];
                        Object o = parameterClazz.newInstance();
                        for (Map.Entry<String, Object> entry : map.entrySet()) {
                            String upCaseName = entry.getKey().substring(0, 1).toUpperCase() + entry.getKey().substring(1);
                            Method method = parameterClazz.getDeclaredMethod("set" + upCaseName, entry.getValue().getClass());
                            method.invoke(o,entry.getValue());
                        }
                        Object result = declaredMethod.invoke(reflectTest, o);
                        System.out.println(result);
                    }
                }
            }
        }

    }
    //controller
    @RequestMapping("/save")
    public static String save(Student student){
        return student.toString();
    }
//    public static Class getClassMethod(){
//
//    }
}
