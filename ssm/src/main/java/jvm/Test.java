package jvm;

public class Test {
    public static void main(String[] args) {
        new Test().test();
    }

    /**
     * 递归调用 演示栈溢出
     * 使用递归时需注意递归的深度
     */
    public void test(){
        test();
    }
}
