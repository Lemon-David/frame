package entity;

import aop.LogData;
import aop.LogType;

import java.io.Serializable;

public class Student implements Serializable,Cloneable {
    private Integer id;
    //模拟属性名与列名不一致的情况
    @LogData(logType = LogType.LOGIN,description = "属性注解测试")
    private String name;
    private Integer classid;
    private Integer version;
    private StudentInfo studentInfo;
    private Integer age;
    private String address;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public Student() {
        System.out.println("调用无参构造创建对象");
    }

    public Student(Integer id, String name, Integer classid, Integer version, StudentInfo studentInfo) {
        this.id = id;
        this.name = name;
        this.classid = classid;
        this.version = version;
        this.studentInfo = studentInfo;
        System.out.println("调用有参构造创建对象");
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getClassid() {
        return classid;
    }

    public void setClassid(Integer classid) {
        this.classid = classid;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public StudentInfo getStudentInfo() {
        return studentInfo;
    }

    public void setStudentInfo(StudentInfo studentInfo) {
        this.studentInfo = studentInfo;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", classid=" + classid +
                ", version=" + version +
                ", studentInfo=" + studentInfo +
                '}';
    }
}
