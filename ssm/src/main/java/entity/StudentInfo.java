package entity;

import java.io.Serializable;

public class StudentInfo implements Serializable,Cloneable {
    //序列化版本号，版本号相同时，能够被反序列化成功，否则反序列化失败
    private static final long serialVersionUID = 6158832819921615320L;
    private Integer id;
    //被static修饰的不会被序列化
    private static String address;
    //被transient修饰的不会被序列化
    private transient Integer age;

    public StudentInfo() {
    }

    public StudentInfo(Integer id, String address, Integer age) {
        this.id = id;
        this.address = address;
        this.age = age;
    }

    @Override
    public String toString() {
        return "StudentInfo{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", age=" + age +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
