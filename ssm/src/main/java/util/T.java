package util;

import aop.LogAspect;
import aop.LogType;

import static aop.LogType.INSERT;

public class T {
    public static final int LOGTYPE_LOGIN=1;
    public static final int LOGTYPE_DELETE=2;

    public static void main(String[] args) {
        test(LogType.DELETE);
        LogType logType = LogType.DELETE;
        int type = logType.getType();
//        LogType LogType = new LogType();
    }

    public static void test(LogType logType) {
        switch (logType) {
            case DELETE:
                System.out.println("删除操作");
                break;
            case INSERT:
                System.out.println("");
                break;
        }
    }
}
