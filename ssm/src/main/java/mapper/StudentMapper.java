package mapper;

import entity.Student;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface StudentMapper {
    List<Student> selectAll();
    List<Student> selectWithStudentInfoMapping(String name);
    int delete(Integer id);
    List<Student> selectByMap(Map<String, Object> map);
    int update(Student student);
    int insert(Student student);
    Student findById(Integer id);

    int insertAll(Student student,String description,String s);

}
