package mapper;

import entity.StudentInfo;

public interface StudentInfoMapper {
    int insert(StudentInfo studentInfo);
}
