package service;

import entity.StudentInfo;
import mapper.StudentInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentInfoServiceImpl implements StudentInfoService {
    @Autowired
    private StudentInfoMapper studentInfoMapper;
    @Override
    public int insert(StudentInfo studentInfo) {
        return studentInfoMapper.insert(studentInfo);
    }
}
