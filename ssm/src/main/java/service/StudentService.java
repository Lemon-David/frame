package service;

import com.github.pagehelper.PageInfo;
import entity.Student;

import java.util.List;
import java.util.Map;

public interface StudentService {
    /**
     * 根据ID删除
     * @param id
     * @return
     */
    int delete(Integer id);

    /**
     * 查询所有
     * @return
     */
    List<Student> findAll();

    /**
     * 根据map查询条件查询
     * @param map
     * @return
     */
    List<Student> findByMap(Map<String,Object> map);

    /**
     * 分页查询
     * @param map 查询条件
     * @return PageInfo对象
     */
    PageInfo<Student> findByPage(Map<String,Object> map);

    /**
     * 修改
     * @param id
     * @return
     */
    /*int update(Integer id,String name,Integer classid,Integer version);
    int insert(Integer id,String name,Integer classid,Integer version);*/

    Student findById(Integer id);

    int saveOrUpdate(Student student);

    int insertAll(Student student,String description,String s);

}
