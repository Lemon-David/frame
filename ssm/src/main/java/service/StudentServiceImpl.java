package service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import entity.Student;
import mapper.StudentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class StudentServiceImpl implements StudentService{
    @Autowired
    private StudentMapper studentMapper;
    @Autowired
    private StudentInfoService studentInfoService;
    @Override
    public int delete(Integer id) {
        int i=studentMapper.delete(id);
        return i;
    }

    @Override
    public List<Student> findAll() {
        return studentMapper.selectAll();
    }

    @Override
    public List<Student> findByMap(Map<String, Object> map) {
        List<Student> students=studentMapper.selectByMap(map);
        return students;
    }

    @Override
    public PageInfo<Student> findByPage(Map<String, Object> map) {
        //1.设置当前页和每页记录条数
        Integer pageNum = map.get("pageNum")!=null?
                Integer.valueOf(map.get("pageNum").toString()):1;
        Integer pageSize = map.get("pageSize")!=null?
                Integer.valueOf(map.get("pageSize").toString()):2;
        PageHelper.startPage(pageNum,pageSize);
        //2.执行相关查询，自动会转换为分页查询
        List<Student> students = studentMapper.selectByMap(map);
        //3.创建pageInfo对象，传入查询结果
        PageInfo<Student> pageInfo = new PageInfo<>(students);
        return pageInfo;
    }

    /*@Override
    public int update(Integer id,String name,Integer classid,Integer version) {
        int i = studentMapper.update(id,name,classid,version);
        return i;
    }*/

    /*@Override
    public int insert(Integer id, String name, Integer classid, Integer version) {
        int i=studentMapper.insert(id,name,classid,version);
        return i;
    }*/

    @Override
    public Student findById(Integer id) {
        return studentMapper.findById(id);
    }
    //以注解形式配置事务
    @Transactional(propagation = Propagation.REQUIRED,
            isolation = Isolation.READ_COMMITTED)
    @Override
    public int saveOrUpdate(Student student) {
        //开启事务
        if (null==student.getId()){//新增
            //保存student
            studentMapper.insert(student);
//            int j=1/0;
            //保存studentInfo
            //设置id值
            student.getStudentInfo().setId(student.getId());
            int i = 0;
            try {
//                int j=1/0;
                i = studentInfoService.insert(student.getStudentInfo());
            }catch (Exception e){
                e.printStackTrace();
            }
//            int j=1/0;
            return i;
        }
        return studentMapper.update(student);
        //提交事务
    }

    @Override
    public int insertAll(Student student,String description,String s) {
        return studentMapper.insertAll(student,description,s);
    }
}
