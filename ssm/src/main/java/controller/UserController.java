package controller;

import aop.LogData;
import aop.LogType;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import entity.Student;
import entity.User;
import mapper.StudentMapper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import service.StudentService;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * 控制层==>Service层（处理业务逻辑的）==>调用dao层
 */
@Controller
@RequestMapping("/user")
public class UserController {
    //定义日志对象
    private static final Logger logger=LogManager.getLogger(UserController.class);
    @Autowired
    private StudentService studentService;
    @RequestMapping("/list")//@ModelAttribute 向request域中存值
    public String list(ModelMap modelMap, @RequestParam HashMap<String,Object> map){
        logger.debug("前台传递的查询条件："+map);
        logger.info("info级别日志："+map);
//        List<Student> students = studentService.findAll();
//        List<Student> students = studentService.findByMap(map);
//        modelMap.put("students",students);
        PageInfo<Student> page = studentService.findByPage(map);
        logger.error("查询到分页数据："+page);
        modelMap.put("page",page);
        modelMap.put("params",map);//将查询条件回传到页面，用于回显查询条件
        return "list.jsp";
    }


    @LogData(logType = LogType.DELETE,description = "学生信息删除")
    @RequestMapping("/delete")
    public String delete(Integer id){
        studentService.delete(id);
        return "redirect:list";//重定向到列表请求
    }

    @LogData(logType = LogType.INSERT,description = "学生信息新增")
    @RequestMapping("/save")
    public String saveOrUpdate(Student student){
        //根据是否存在id值，来判断是执行新增还是修改操作
        int i=studentService.saveOrUpdate(student);
        return "redirect:list";
    }

    @LogData(logType = LogType.UPDATE,description = "学生信息修改")
    @RequestMapping("/update")
    public String update(Integer id,ModelMap modelMap){
        //查询用户信息,展示到页面
        Student student=studentService.findById(id);
        modelMap.put("student",student);
        return "update.jsp";
    }

    @RequestMapping("/toAdd")
    public String toAdd(){
        return "add.jsp";
    }

    /*@RequestMapping("/insert")
    public String insert(Integer id,String name,Integer classid,Integer version){
        studentService.insert(id,name,classid,version);
        return "redirect:list";
    }*/

    @RequestMapping("/updateTo")
    public String updateTo(Integer id,String name,Integer classid,Integer version,ModelMap modelMap){
        modelMap.put("id",id);
        modelMap.put("name",name);
        modelMap.put("classid",classid);
        modelMap.put("version",version);
        return "update.jsp";
    }

    @Autowired
    private StudentMapper studentMapper;
    @ResponseBody//返回json数据
    @RequestMapping("/select")
    public List<Student> select(@RequestBody String name){
        String names = name.substring(9,11);
        List<Student> students = studentMapper.selectWithStudentInfoMapping(names);
        return students;
    }

    @RequestMapping("/login")
    public String login(String name, String password, HttpSession session){
        System.out.println("获取到前台数据：name="+name+"，password="+password);
        User user = new User();
        user.setId(1);
        user.setName("路飞");
        user.setPassword("ssss");
        System.out.println(user);
        if (user==null){
            return "/user/login.jsp";
        }
        session.setAttribute("user",user);
        return "/index.jsp";
    }
}
