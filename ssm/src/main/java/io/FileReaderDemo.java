package io;

import java.io.*;

/**
 * 1、创建FileInputStream对象，指定要读的文件（写好文件路径）
 * 2、创建InputStreamReader，将创建好的FileInputStream对象加入并指定编码格式，一般为UTF-8
 * 3、new一个char数组，大小一般为1024
 * 4、写一个while循环，利用创建的InputStreamReader对象里的read方法把读出来的放到char数组中，并判断当等于-1时，停止读。
 * 5、利用new String输出读出的内容，偏离量为0，大小为读到的大小。
 */
public class FileReaderDemo {
    public static void main(String[] args) {
        //字节流转字符流
        try(
                FileInputStream fis = new FileInputStream("D:/Test/iotest/a.txt");
                //将字节流转换成字符流，并设定字符编码格式
                InputStreamReader reader=new InputStreamReader(fis,"UTF-8");
                FileOutputStream fos = new FileOutputStream("D:/Test/iotest/c.txt");
                //将字节输出流转换成字符输出流
                OutputStreamWriter writer = new OutputStreamWriter(fos,"UTF-8");
        ){
            char[] chars = new char[1024];
            int size=0;
            while ((size=reader.read(chars))!=-1){
                System.out.println(new String(chars,0,size));
                writer.write(chars,0,size);//以字符形式写入文件
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
