package io;

import jdk.nashorn.internal.ir.CallNode;

import java.io.*;
import java.util.Arrays;

/**
 * 文件字节流操作
 */
public class FileIoDemo {
    public static void main(String[] args) {
        //将流对象定义在try()中，程序执行完成之后会自动关闭流资源
        try(FileInputStream is= new FileInputStream("D:/Test/iotest/a.txt");
            //字节缓冲流：内部维护了一个默认8192字节缓冲区，可以提供读取效率。需要传入字节流
            BufferedInputStream bis=new BufferedInputStream(is);
            FileOutputStream fos = new FileOutputStream("D:/Test/iotest/b.txt");
            BufferedOutputStream bos=new BufferedOutputStream(fos);
        ) {
            byte[] bytes = new byte[1024];//1
            int size=0;
            while ((size=bis.read(bytes))!=-1){//循环读取文件内容，返回-1.则表示读取到文件末尾
                System.out.println(Arrays.toString(bytes));
                //写入文件，bytes写入的数组，0数组的起始位置，size写入的字节大小
                bos.write(bytes,0,size);
            }
            bos.flush();//清空缓存区数据
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
