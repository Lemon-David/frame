package io;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;

/**
 * 演示File操作
 * java中通过File类来表示文件
 */
public class FileDemo {
    public static void main(String[] args) throws IOException {
        File file = new File("D:/Test/iotest");
        boolean exists = file.exists();
        System.out.println(exists);
        if (!exists){
            //创建空的文件，不能写，如要要写通过ID流
            file.mkdirs();//创建当前目录和父目录
        }
        File file1 = new File(file, "a.txt");
        System.out.println(file1.getName());
        if (!file1.exists()){
            file1.createNewFile();
        }
        long length = file1.length();//获取到文件大小
        System.out.println(length);
        //lambda
        String[] list = file.list((File dir,String name)->name.endsWith(".java"));
//        String[] list = file.list(new FilenameFilter() {
//            @Override
//            public boolean accept(File dir, String name) {
//                return name.endsWith(".java");
//            }
//        });
        System.out.println(Arrays.toString(list));
    }
}
