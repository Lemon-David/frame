package io;

import entity.Student;
import entity.StudentInfo;
import jdk.nashorn.internal.ir.CallNode;

import java.io.*;

/**
 * 演示对象的序列化与反序列化操作
 * 前提：对象需要实现序列化接口
 * 序列化：将java对象转换成字节序列的过程我们称之为序列化 通过ObjectOutputStream来实现
 * 反序列化：将字节序列转换称java对象的过程我们称之为反序列化 通过ObjectInputStream来实现
 *
 * 如果某些属性不希望被序列化
 * 1）transient修饰的属性
 * 2）static修饰的属性
 */
public class ObjectStreamDemo {
    public static void main(String[] args) {
        StudentInfo studentInfo = new StudentInfo(1, "南昌", 18);
        Student student = new Student(1, "james", 1, 1, studentInfo);
        try(    //输出流，用来指定对象字节的输出方式
                FileOutputStream fos = new FileOutputStream("D:/Test/iotest/student.txt");
                //对象输出流，将对象转换成字节序列，并通过fos输出
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                FileInputStream fis=new FileInputStream("D:/Test/iotest/student.txt");
                ObjectInputStream ois = new ObjectInputStream(fis);
        ) {
            //序列化对象
            oos.writeObject(student);
            oos.flush();
            System.out.println("=======序列化成功=========");
            Object o = ois.readObject();
            if (o instanceof Student){
                Student student1 = (Student) o;
                System.out.println("=======反序列化成功=========");
                System.out.println(student==student1);//false
                System.out.println(student1.getStudentInfo());
                //深拷贝，克隆对象的引用数据类型的属性与原对象的属性为不同对象
                System.out.println(student.getStudentInfo()==student1.getStudentInfo());//false
                System.out.println(student.getId().equals(student1.getId()));//true
                System.out.println(student.getStudentInfo().equals(student1.getStudentInfo()));//false 比的是对象，地址值
                System.out.println("======clone创建对象========");
                //通过克隆创建对象
                Student student2 = (Student) student.clone();
                System.out.println(student2==student);//false
                //浅拷贝，克隆对象的引用数据类型的属性与原对象的属性为同一对象，只是地址的拷贝。
                System.out.println(student2.getStudentInfo()==student.getStudentInfo());//true

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
