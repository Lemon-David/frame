package data;

public class LinkedNode<T> {
    //节点数据
    private T data;
    //下一个节点引用
    private LinkedNode<T> next;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public LinkedNode<T> getNext() {
        return next;
    }

    public void setNext(LinkedNode<T> next) {
        this.next = next;
    }
}
