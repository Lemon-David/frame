package thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ThreadDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //1.继承Thread类，重写Run方法
        Thread thread = new Thread(){//ctrl+o 重写方法
            @Override
            public void run() {
                System.out.println("通过继承Thread类，并重写run方法，创建线程");
            }
        };
        //2.实现Runnable接口，将runnable对象传入Thread中,匿名内部类
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("实现runnable接口创建线程");
            }
        });
        thread.start();
        thread2.start();
        //3.创建线程池方式（推荐）
        //1.可缓存的线程池，最大线程数量为Integer.MAX_VALUE
        ExecutorService threadPool = Executors.newCachedThreadPool();
        //2.单线程线程池，只有一个线程
        threadPool = Executors.newSingleThreadExecutor();
        //3.创建定长线程池，在构造方法中传入线程的数量，如果线程池中线程都已使用，则任务需要等待线程执行完成再执行
        threadPool= Executors.newFixedThreadPool(10);
        //自己定义线程池
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 20,
                160L, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
        //4.周期性执行的线程
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(10);
        //延迟周期性执行任务，可以实现简单的定时任务
        scheduledExecutorService.scheduleWithFixedDelay(
                ()-> System.out.println(System.currentTimeMillis()),//runnable任务
                1,//第一次执行的延迟时间
                2,//每隔多长时间执行一次
                TimeUnit.SECONDS//时间单位
                );
        //延迟指定时间执行对应的任务，只执行一次
        scheduledExecutorService.schedule(
                ()-> System.out.println("schedule方法执行时间："+System.currentTimeMillis()),
                3,
                TimeUnit.SECONDS
                );
        //execute 执行runnable任务，需传入一个runnable对象
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("通过线程池获取的线程来执行runnable任务");
            }
        });
        //submit方法执行runnable任务
        Future<?> future = threadPool.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("通过线程池的submit方法执行runnable任务");
            }
        });
        //submit执行Callable任务，Callable可以有返回值，线程执行完成之后，可通过future对象对值进行获取
        Future<Object> future1 = threadPool.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                System.out.println("睡眠10秒");
                Thread.sleep(10000);
                return "Callable方法的返回值";
            }
        });
        //获取线程返回值，会阻塞等待线程执行完毕返回结果
        Object o = future1.get();
        System.out.println("获取到返回值"+o);
//        System.exit(0);//正常退出程序 非0表示非正常退出程序 return是回到上一层，而System.exit(status)是回到最上层
        List<Callable<String>> taskList=new ArrayList<>();
        /*Callable stringCallable = new Callable() {
            @Override
            public Object call() throws Exception {
                return "callable任务一";
            }
        };*/
        taskList.add(()->"callable任务一");
        taskList.add(()->"callable任务二");
        //执行多个任务
        List<Future<String>> futures = threadPool.invokeAll(taskList);
        for (Future<String> stringFuture : futures) {
            String s = stringFuture.get();
            System.out.println("返回结果"+s);
        }
    }
}
