package proxy.dynamicproxy;

public class Bigpeng implements Coder {
    @Override
    public void code() {
        System.out.println("打代码");
    }

    @Override
    public void debug() {
        System.out.println("调bug");
    }
}
