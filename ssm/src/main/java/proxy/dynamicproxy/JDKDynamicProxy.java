package proxy.dynamicproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * jdk动态代理
 */
public class JDKDynamicProxy implements InvocationHandler {
    //持有一个被代理对象
    private Object object;

    /**
     * 根据被代理对象生成代理类
     * @param targetObject 被代理的对象
     * @return
     */
    public Object getInstance(Object targetObject){
        //给被代理对象赋值
        this.object=targetObject;
        //生成代理对象
        return Proxy.newProxyInstance(targetObject.getClass().getClassLoader(),//获取类加载器
                targetObject.getClass().getInterfaces(),//获取被代理类实现的接口
                this//动态代理方法执行时，会调用该对象的invoke方法
        );
    }
    /**
     * 调用代理对象的方法
     * @param proxy 代理对象
     * @param method 调用的方法
     * @param args 调用方法传递的参数
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("开启事务");
        //调用被代理对象的方法
        Object invoke = method.invoke(object, args);
        System.out.println("提交事务");
        return invoke;
    }
}
