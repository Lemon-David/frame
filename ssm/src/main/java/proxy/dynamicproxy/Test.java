package proxy.dynamicproxy;

import proxy.staticproxy.Liudehua;
import proxy.staticproxy.Star;

public class Test {
    public static void main(String[] args) {
        JDKDynamicProxy jdkDynamicProxy = new JDKDynamicProxy();
        //被代理类
        Liudehua liudehua = new Liudehua();
        //动态生成代理类对象
        Star instance = (Star) jdkDynamicProxy.getInstance(liudehua);
        instance.actor();
        instance.sing();

        //1.创建被代理对象
        Bigpeng bigpeng = new Bigpeng();
        //2.动态生成代理对象
        Coder instance1 = (Coder) jdkDynamicProxy.getInstance(bigpeng);
        instance1.code();
        instance1.debug();

        //cglib动态代理
        //创建被代理对象
        Player player = new Player();
        //创建生产动态代理对象的对象
        CglibDynamicProxy cglibDynamicProxy = new CglibDynamicProxy();
        Player proxy = (Player) cglibDynamicProxy.getProxy(player);
        proxy.play();
    }
}
