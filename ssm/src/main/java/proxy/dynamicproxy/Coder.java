package proxy.dynamicproxy;

public interface Coder {
    void code();
    void debug();
}
