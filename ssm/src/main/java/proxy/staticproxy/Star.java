package proxy.staticproxy;

/**
 * 代理类与被代理类实现的统一接口
 */
public interface Star {
    void actor();
    void sing();
}
