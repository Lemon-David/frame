package proxy.staticproxy;

public class StarProxy implements Star{
    //持有一个被代理对象
    private Star liudehua;

    public StarProxy(Star liudehua) {
        this.liudehua = liudehua;
    }

    @Override
    public void actor() {
        System.out.println("经纪人谈合同");
        liudehua.actor();
        System.out.println("经纪人收钱");
    }

    @Override
    public void sing() {
        System.out.println("经纪人联系场地");
        liudehua.sing();
        System.out.println("经纪人收钱");
    }
}
