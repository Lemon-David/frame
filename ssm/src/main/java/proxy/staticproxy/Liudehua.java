package proxy.staticproxy;

/**
 * 被代理类
 */
public class Liudehua implements Star {
    @Override
    public void actor() {
        System.out.println("刘德华演戏");
    }

    @Override
    public void sing() {
        System.out.println("刘德华唱歌");
    }
}
