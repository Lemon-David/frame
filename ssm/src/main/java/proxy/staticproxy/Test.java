package proxy.staticproxy;

public class Test {
    public static void main(String[] args) {
        //创建被代理对象
        Star liudehua = new Liudehua();
        //创建代理对象，并传入被代理对象
        Star starProxy = new StarProxy(liudehua);
        starProxy.sing();
        starProxy.actor();
    }

    public static void show(Star star){
        star.sing();
        star.actor();
    }
}
