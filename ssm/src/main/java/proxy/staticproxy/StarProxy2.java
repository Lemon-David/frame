package proxy.staticproxy;

public class StarProxy2 extends Liudehua{
    @Override
    public void actor() {
        System.out.println("经纪人谈价钱");
        super.actor();
        System.out.println("经纪人收钱");
    }

    @Override
    public void sing() {
        super.sing();
    }
}
