-- 部门表
create table t_dept(
   id number(11) primary key ,
   deptcode varchar2(20) unique not null ,
   deptname varchar2(50) not null
);
-- 管理员表
create table t_admin(
    id number(11) primary key ,
    account varchar2(50) unique not null ,
    password varchar2(20) not null ,
    deptid number(11) references t_dept(id)
);
-- 角色表
create table t_role(
    id number(11) primary key ,
    rolecode varchar2(50) not null,
    rolename varchar2(50) not null ,
    status char(1) default '0'
);
-- 用户角色关系表(生成实体类时，此表不需要操作)
create  table t_user_role(
    roleid number(11) references t_role(id)  not null ,
    userid number(11) references t_admin(id) not null
);
-- 用户详情表
create table t_user_detail(
    id number(11) primary key ,
    addr varchar2(200),
    education varchar2(20)
);

create sequence seq_t_admin;
create sequence seq_t_role;
create sequence seq_t_dept;