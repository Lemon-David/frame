import com.seecen.hibernate.entity.User;
import com.seecen.hibernate.util.DBUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

import java.util.Date;

/**
 * @author bigpeng
 * @create 2020-05-07-14:15
 */
public class UserTest {

    @Test
    public void test(){
        //1. 加载配置文件
        Configuration configure = new Configuration().configure("/hibernate.cfg.xml");
        //2.创建SessionFactory
        SessionFactory sessionFactory = configure.buildSessionFactory();
        //3.获取session对象 session对象是hibernate用来执行数据库操作的对象，类似于Connection
        Session session = sessionFactory.openSession();
        //4.开启事务
        Transaction transaction = session.beginTransaction();
        // 5.通过session执行数据库操作
        User user = new User("张三丰","0",18,new Date());
        //插入用户数据
        session.save(user);
        //6.提交事务
        transaction.commit();
        //关闭session
        session.close();
        //关闭sessionFactory
        sessionFactory.close();
    }
    //根据主键进行查询get和load
    @Test
    public void selectTest(){
        Session session = DBUtil.getSession();
        //session.get 根据主键进行查询 (实体类,主键值)，
        // 会立即执行sql，不管查询到的对象是否被使用
        User user = session.get(User.class, 2);
        System.out.println(user);
        System.out.println("=========分割线==========");
        //load 根据主键查询，会延迟加载，
        // 只有在使用到查询对象的时候才会去执行查询
//        User user2 = session.load(User.class, 1);
//        System.out.println(user2);
        DBUtil.close(session,DBUtil.sessionFactory);
    }

    @Test
    public void update(){
        Session session = DBUtil.getSession();
        Transaction transaction = session.beginTransaction();
//        User user = new User(1,"张无忌","1",28,new Date());
        User user=new User();
        user.setId(2);
        //更新，传入需要更新的对象，对象必须包含主键
//        session.update(user);
        session.delete(user);//删除
        transaction.commit();
        DBUtil.close(session,DBUtil.sessionFactory);

    }

}
