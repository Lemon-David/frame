import com.seecen.hibernate.entity.Admin;
import com.seecen.hibernate.entity.Dept;
import com.seecen.hibernate.entity.Role;
import com.seecen.hibernate.entity.UserDetail;
import com.seecen.hibernate.util.DBUtil;
import com.sun.media.sound.SoftTuning;
import jdk.nashorn.internal.codegen.DumpBytecode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author bigpeng
 * @create 2020-05-08-10:50
 */
public class AdminTest {

    @Test
    public void oneToOne(){
        Session session = DBUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Admin admin = new Admin("张三丰","123456");
        UserDetail userDetail = new UserDetail("南昌市","本科");
        admin.setUserDetail(userDetail);
        userDetail.setAdmin(admin);
        session.save(admin);
        transaction.commit();
        DBUtil.close(session,DBUtil.sessionFactory);
    }
    @Test
    public void oneToOneSelect(){
        Session session = DBUtil.getSession();
        Admin admin = session.get(Admin.class, 2L);
        System.out.println(admin);
        DBUtil.close(session,DBUtil.sessionFactory);
    }

    @Test
    public void oneToOneUpdate(){
        Session session = DBUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Admin admin = session.get(Admin.class, 2L);
        admin.setAccount("张无忌");
        admin.getUserDetail().setAddr("光明顶");
        session.update(admin);
        transaction.commit();
        DBUtil.close(session,DBUtil.sessionFactory);
    }
    @Test
    public void oneToOneDelete(){
        Session session = DBUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Admin admin = new Admin();
        admin.setId(9L);
        UserDetail userDetail = new UserDetail();
        userDetail.setId(9L);
        userDetail.setAdmin(admin);
        admin.setUserDetail(userDetail);
        session.delete(admin);
        transaction.commit();
        DBUtil.close(session,DBUtil.sessionFactory);
    }
    public  void test(){
        ArrayList list=new ArrayList();
        //将线程不安全的集合转化成线程安全的
        List list1 = Collections.synchronizedList(list);
        list1.add("A");
    }

    @Test
    public void oneToMany(){
        Session session = DBUtil.getSession();
//        Transaction transaction = session.beginTransaction();
//        Dept dept = new Dept("sc001","后勤部");
//        session.save(dept);
//        transaction.commit();
        Dept dept = session.get(Dept.class, 1L);
        System.out.println(dept);
        DBUtil.close(session,DBUtil.sessionFactory);

    }

    @Test
    public void manyToOne(){
        Session session = DBUtil.getSession();
        //关联查询
        Admin admin = session.get(Admin.class, 1L);
        System.out.println(admin);
        System.out.println(admin.getDept());
        //级联保存
//        Transaction transaction = session.beginTransaction();
//        Admin admin1 = new Admin("zhang","123");
//        UserDetail userDetail = new UserDetail("丰城", "博士");
//        Dept dept = new Dept("002", "就业部");
//        admin1.setDept(dept);
//        admin1.setUserDetail(userDetail);
//        userDetail.setAdmin(admin1);//一对一，userDetail主键来自于Admin
//        session.save(admin1);
//        transaction.commit();//提交事务
        DBUtil.close(session,DBUtil.sessionFactory);

    }

    @Test
    public void manyToMany(){
        Session session = DBUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Admin admin1 = new Admin("test","123");
        List<Role> roles = Arrays.asList(new Role("004","java开发员","0")
                , new Role("005","美图员","0"));
        admin1.setRoleList(roles);
        session.save(admin1);
        transaction.commit();
        DBUtil.close(session,DBUtil.sessionFactory);
    }
    @Test
    public void manyToManyQuery(){
        Admin admin = DBUtil.getSession().get(Admin.class, 11L);
        System.out.println(admin);
        System.out.println(admin.getRoleList());
    }

}
