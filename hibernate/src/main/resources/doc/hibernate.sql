-- 部门表
create table t_dept(
   id number(11) primary key ,
   deptcode varchar2(20) unique not null ,
   deptname varchar2(50) not null
);
-- 管理员表
create table t_admin(
    id number(11) primary key ,
    account varchar2(50) unique not null ,
    password varchar2(20) not null ,
    deptid number(11) references t_dept(id)
);
-- 角色表
create table t_role(
    id number(11) primary key ,
    rolecode varchar2(50) not null,
    rolename varchar2(50) not null ,
    status char(1) default '0'
);
-- 用户角色关系表(生成实体类时，此表不需要操作)
create  table t_user_role(
    roleid number(11) references t_role(id)  not null ,
    userid number(11) references t_admin(id) not null
);
-- 用户详情表
create table t_user_detail(
    id number(11) primary key ,
    addr varchar2(200),
    education varchar2(20)
);

create sequence seq_t_admin;
create sequence seq_t_role;
create sequence seq_t_dept;





-- 老师表
create table h_teacher(
    id number(11) primary key,
    name varchar2(50)
);

-- 班级表
create table h_class(
    id number(11) primary key,
    name varchar2(50)
);

-- 学生表
create table h_student(
    id number(11) primary key,
    name varchar2(50) unique not null,
    classid number(11) references h_class(id)
);

-- 学生详细表
create table h_student_info(
    id number(11) primary key,
    age number(4),
    address varchar2(200)
);

-- 班级老师关系表
create table h_class_teacher(
    classid number(11) references h_class(id) not null,
    teacherid number(11) references h_teacher(id) not null
);

create sequence seq_h_student;
create sequence seq_h_teacher;
create sequence seq_h_class;