package com.seecen.hibernate.entity;

import java.util.List;

/**
 * @author bigpeng
 * @create 2020-05-08-10:09
 */
public class Dept {
    private long id;
    private String deptcode;
    private String deptname;
    /**
     *   一对多
     */
    private List<Admin> adminList;

    @Override
    public String toString() {
        return "Dept{" +
                "id=" + id +
                ", deptcode='" + deptcode + '\'' +
                ", deptname='" + deptname + '\'' +
                ", adminList=" + adminList +
                '}';
    }

    public Dept() {
    }

    public Dept(String deptcode, String deptname) {
        this.deptcode=deptcode;
        this.deptname=deptname;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDeptcode() {
        return deptcode;
    }

    public void setDeptcode(String deptcode) {
        this.deptcode = deptcode;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dept dept = (Dept) o;

        if (id != dept.id) return false;
        if (deptcode != null ? !deptcode.equals(dept.deptcode) : dept.deptcode != null) return false;
        if (deptname != null ? !deptname.equals(dept.deptname) : dept.deptname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (deptcode != null ? deptcode.hashCode() : 0);
        result = 31 * result + (deptname != null ? deptname.hashCode() : 0);
        return result;
    }

    public List<Admin> getAdminList() {
        return adminList;
    }

    public void setAdminList(List<Admin> adminList) {
        this.adminList = adminList;
    }
}
