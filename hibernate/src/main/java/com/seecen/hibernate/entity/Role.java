package com.seecen.hibernate.entity;

/**
 * @author bigpeng
 * @create 2020-05-08-10:09
 */
public class Role {
    private long id;
    private String rolecode;
    private String rolename;
    private String status;

    public Role() {
    }

    public Role(String rolecode,String rolename,String status){
        this.rolecode=rolecode;
        this.rolename=rolename;
        this.status=status;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRolecode() {
        return rolecode;
    }

    public void setRolecode(String rolecode) {
        this.rolecode = rolecode;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        if (id != role.id) return false;
        if (rolecode != null ? !rolecode.equals(role.rolecode) : role.rolecode != null) return false;
        if (rolename != null ? !rolename.equals(role.rolename) : role.rolename != null) return false;
        if (status != null ? !status.equals(role.status) : role.status != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (rolecode != null ? rolecode.hashCode() : 0);
        result = 31 * result + (rolename != null ? rolename.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", rolecode='" + rolecode + '\'' +
                ", rolename='" + rolename + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
