package com.seecen.hibernate.entity;

import java.util.Date;

/**
 * @author bigpeng
 * @create 2020-05-07-10:36
 */
public class User {
    private String name;
    private String sex;
    private Integer age;
    private Date birthday;
    private Integer id;


    public User() {
    }

    public User(String name,String sex,Integer age,Date birthday ) {
        this.age=age;
        this.birthday=birthday;
        this.sex=sex;
        this.name=name;
    }
    public User(Integer id,String name,String sex,Integer age,Date birthday ) {
        this.id=id;
        this.age=age;
        this.birthday=birthday;
        this.sex=sex;
        this.name=name;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if (sex != null ? !sex.equals(user.sex) : user.sex != null) return false;
        if (age != null ? !age.equals(user.age) : user.age != null) return false;
        if (birthday != null ? !birthday.equals(user.birthday) : user.birthday != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        result = 31 * result + (age != null ? age.hashCode() : 0);
        result = 31 * result + (birthday != null ? birthday.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", birthday=" + birthday +
                ", id=" + id +
                '}';
    }
    // alt+insert

}
