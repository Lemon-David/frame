package com.seecen.hibernate.entity;

/**
 * @author bigpeng
 * @create 2020-05-08-10:09
 */
public class UserDetail {
    private long id;
    private String addr;
    private String education;
    // 一对一
    private Admin admin;


    public UserDetail() {
    }
    public UserDetail(String addr,String education){
        this.addr=addr;
        this.education=education;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDetail that = (UserDetail) o;

        if (id != that.id) return false;
        if (addr != null ? !addr.equals(that.addr) : that.addr != null) return false;
        if (education != null ? !education.equals(that.education) : that.education != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (addr != null ? addr.hashCode() : 0);
        result = 31 * result + (education != null ? education.hashCode() : 0);
        return result;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    @Override
    public String toString() {
        return "UserDetail{" +
                "id=" + id +
                ", addr='" + addr + '\'' +
                ", education='" + education + '\'' +
                '}';
    }
}
