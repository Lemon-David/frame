import com.seecen.mybatis2.mapper.ClassMapper;
import com.seecen.mybatis2.mapper.StudentInfoMapper;
import com.seecen.mybatis2.mapper.StudentMapper;
import com.seecen.mybatis2.mapper.TeacherMapper;
import com.seecen.mybatis2.pojo.Class;
import com.seecen.mybatis2.pojo.Student;
import com.seecen.mybatis2.pojo.StudentInfo;
import com.seecen.mybatis2.pojo.Teacher;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ClassTest {
    SqlSession sqlSession;
    @Before
    public void before() throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        sqlSession = sqlSessionFactory.openSession();
    }
    @Test
    public void classTest() throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        ClassMapper mapper = sqlSession.getMapper(ClassMapper.class);
        //通过id查Class表中指定数据
        Class aClass = mapper.selectById(21);
        System.out.println(aClass);
        //查Class表中所有数据
        List<Class> classes = mapper.selectAll();
        for (Class aClass1 : classes) {
            System.out.println(aClass1);
        }
        //通过id删除指定数据
        int delete = mapper.deleteById(100);
        System.out.println("删除记录数："+delete);
//        Class aClass1 = new Class();
//        aClass1.setName("大二");
//        int insert = mapper.insert(aClass1);
//        System.out.println("插入记录数："+insert);
//        System.out.println(aClass1);
        aClass.setName("大三");
        int update = mapper.update(aClass);
        System.out.println("修改记录数："+update);
        sqlSession.commit();
        sqlSession.close();
        is.close();
    }
    @Test
    public void classTest2() throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        ClassMapper mapper = sqlSession.getMapper(ClassMapper.class);
        //测试${}取值
        List<Class> classes = mapper.selectAllList("id");
        for (Class aClass : classes) {
            System.out.println(aClass);
        }
        //多个参数的取值方式，@Param取别名
        List<Class> classList = mapper.selectByIdOrName(21, "大四");
        System.out.println(classList);
        //传递map 通过#{key} 取value
        HashMap<String, Object> map = new HashMap<>();
        map.put("id",21);
        map.put("name","大四");
        List<Class> classes1 = mapper.selectByMap(map);
        System.out.println(classes1);
        //传递数组或集合 批量删除
        int count = mapper.deleteByIds(new Integer[]{2, 3});
        System.out.printf("批量删除%d条记录",count);
        //批量插入
        List<Class> classes2 = Arrays.asList(new Class("初一"), new Class("大一"), new Class("大二"));
        int i = mapper.batchInsert(classes2);
        System.out.printf("批量插入了%d条记录",i);
        sqlSession.commit();
        sqlSession.close();
        is.close();
    }
    @Test
    public void classTest3() throws IOException {
        //choose when otherwise测试
        InputStream is = Resources.getResourceAsStream("mybatis.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        ClassMapper mapper = sqlSession.getMapper(ClassMapper.class);
        List<Class> classes = mapper.selectByIdOrName(1, "大一");
        System.out.println(classes);
        List<Class> classes2 = mapper.selectByIdOrName(null, "大一");
        System.out.println(classes2);
        //选择性更新 set标签会动态插入set关键字，并删除额外的逗号
        Class gaoer = new Class("高二");
        gaoer.setId(42);gaoer.setTest(1);
        mapper.updateSelective(gaoer);
        gaoer.setName(null);
        mapper.updateSelective(gaoer);
        gaoer.setName("高三");gaoer.setTest(null);
        mapper.update(gaoer);
        //选择性插入 trim指定以特定字符开始和结束，并去除前后的特定字符
//        Class gaoyi = new Class("高一");
//        gaoyi.setTest(2);
//        mapper.insertSelective(gaoyi);
//        gaoyi.setTest(null);
//        mapper.insertSelective(gaoyi);
        //模糊查询 bind元素允许你在OGNL表达式以外创建一个变量，并将其绑定当前的上下文。常用于模糊查询
        String name="一";
//        List<Class> classes1 = mapper.selectByName("%"+name+"%");
        List<Class> classes1 = mapper.selectByName(name);
        System.out.println(classes1);
        //测试sql和include标签 用sql标签定义sql片段 通过include引入sql片段
        Class aClass = mapper.selectById(42);
        System.out.println(aClass);
        //查询结果的封装是调用对应的实体类的set+列名方法进行数据封装的
        //resultType指定返回的数据类型 要求列名必须与类的属性名一致，如果不一致可对列取别名的方式，使列名与属性名一致
        StudentInfoMapper studentInfoMapper = sqlSession.getMapper(StudentInfoMapper.class);
        StudentInfo studentInfo = studentInfoMapper.selectById(1);
        System.out.println(studentInfo);
        //测试resultMap映射 resultMap指定返回的结果集映射
        StudentInfo studentInfo1 = studentInfoMapper.selectByIdMappingWithResultMap(1);
        System.out.println(studentInfo1);
        sqlSession.commit();
        sqlSession.close();
        is.close();
    }
    @Test
    public void classTest4() throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        //关联查询 association 配置返回单个对象的关联查询
        Student student = mapper.selectByIdMappingWithStudentInfo(8);
        System.out.println(student);
        System.out.println(student.getStudentInfo());
        //collection 配置返回多个对象的关联查询
        ClassMapper mapper1 = sqlSession.getMapper(ClassMapper.class);
        Class aClass = mapper1.selectWithStudent(2);
        System.out.println(aClass.getId()+aClass.getName());
//        System.out.println(aClass);
        System.out.println(aClass.getStudents());
        sqlSession.commit();
        sqlSession.close();
        is.close();
    }
    @Test
    public void classTest5() {
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        Student student = mapper.selectWithStudentInfoMapping(2);
        System.out.println(student);
        System.out.println(student.getStudentInfo());
        TeacherMapper mapper1 = sqlSession.getMapper(TeacherMapper.class);
        Teacher teacher = mapper1.selectWithClassMapping(1);
        System.out.println(teacher);
        System.out.println(teacher.getClasses());
    }

    @After
    public void after(){
        sqlSession.commit();
        sqlSession.close();
    }
}
