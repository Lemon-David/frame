import com.seecen.mybatis2.mapper.ClassMapper;
import com.seecen.mybatis2.mapper.StudentMapper;
import com.seecen.mybatis2.mapper.TeacherMapper;
import com.seecen.mybatis2.pojo.Class;
import com.seecen.mybatis2.pojo.Student;
import com.seecen.mybatis2.pojo.Teacher;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class TeacherTest {
    @Test
    public void selectByIdTest() throws IOException {
        //1.加载配置文件
        InputStream is = Resources.getResourceAsStream("mybatis.xml");
        //2.创建SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        //3.通过sqlSessionFactory获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //4.获取Mapper接口代理对象
        TeacherMapper mapper = sqlSession.getMapper(TeacherMapper.class);
        Teacher teacher = mapper.selectById(3);
        System.out.println(teacher);
        int i = mapper.deleteById(1000);
        System.out.println("删除记录数："+i);
//        Teacher teacher1 = new Teacher();
//        teacher1.setName("周伯通");
//        teacher1.setId(2);
//        int count = mapper.insert(teacher1);
//        System.out.println("插入记录数："+count);
//        System.out.println(teacher1);
        //修改
        teacher.setName("黄蓉");
        int update = mapper.update(teacher);
        System.out.println("修改记录数："+update);
        //测试${}取值
        List<Teacher> teachers = mapper.selectAll("id");
        for (Teacher teacher1 : teachers) {
            System.out.println(teacher1);
        }
        List<Teacher> teacherList = mapper.selectByIdOrName(101, "周伯通");
        System.out.println(teacherList);
        System.out.println("========测试map传值=======");
        //测试传递Map
        HashMap<String, Object> map = new HashMap<>();
        map.put("id",101);
        map.put("name","周伯通");
        List<Teacher> teachers1 = mapper.selectByMap(map);
        System.out.println(teachers1);
        //批量删除
        int count = mapper.deleteByIds(new Integer[]{101, 23});
        System.out.printf("批量删除%d条记录\n",count);
        //事务提交
        sqlSession.commit();
        //5.关闭资源
        sqlSession.close();
        is.close();
    }

    @Test
    public void test2() throws IOException {
        //1.加载配置文件
        InputStream is = Resources.getResourceAsStream("mybatis.xml");
        //2.创建SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        //3.通过sqlSessionFactory获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //4.获取Mapper接口代理对象
        TeacherMapper mapper = sqlSession.getMapper(TeacherMapper.class);
        //批量插入
//        List<Teacher> teachers = Arrays.asList(new Teacher("姚明"), new Teacher("詹姆斯"), new Teacher("科比"));
//        int i = mapper.batchInsert(teachers);//-1
//        System.out.printf("批量插入了%d条记录",i);

        //测试传递Map
        HashMap<String, Object> map = new HashMap<>();
//        map.put("id",41);
//        map.put("name","詹姆斯");
        List<Teacher> teachers1 = mapper.selectByMap(map);
        System.out.println(teachers1);
        System.out.println("===========choose when测试=========");
        List<Teacher> teachers = mapper.selectByIdOrName(1, "科比");
        System.out.println(teachers);
        List<Teacher> teachers2 = mapper.selectByIdOrName(null, "科比");
        System.out.println(teachers2);
        System.out.println("===========选择性更新测试=============");
        Teacher yi = new Teacher("易建联");
        yi.setId(41);yi.setAge(18);
        mapper.updateSelective(yi);
        yi.setName(null);
        mapper.updateSelective(yi);
        yi.setName("王治郅");yi.setAge(null);
        mapper.updateSelective(yi);

//        System.out.println("============选择性插入测试=======");
//        Teacher zhu = new Teacher("朱芳雨");
//        zhu.setAge(18);
//        mapper.insertSelective(zhu);
//        zhu.setAge(null);
//        mapper.insertSelective(zhu);

        //模糊查询
        String name="芳";
//        List<Teacher> teachers3 = mapper.selectByName("%" + name + "%");
        List<Teacher> teachers3 = mapper.selectByName(name);
        System.out.println(teachers3);

        System.out.println("=========测试sql和include标签=======");
        Teacher teacher = mapper.selectById(41);
        System.out.println(teacher);

        //查询结果的封装是调用对应的实体类的set+列名方法来进行数据封装的
        StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);
        Student student = studentMapper.selectById(1);
        System.out.println(student);
        //测试resultMap映射
        System.out.println("==========测试resultMap映射===========");
        Student student1 = studentMapper.selectByIdMappingWithResultMap(1);
        System.out.println(student1);
//        System.out.println("===========测试注解插入============");
//        Student student2 = new Student();
//        student2.setStudentName("乔丹");
//        studentMapper.insert(student2);
        System.out.println("==========测试注解查询=========");
        List<Student> students = studentMapper.selectAll();
        System.out.println(students);
        Student student2 = new Student();
        student2.setId(21);student2.setStudentName("杜兰特");
//        studentMapper.update(student2);

//        int delete =studentMapper.delete(21);

        System.out.println("=======关联查询=========");
        Student student3 = studentMapper.selectByIdMappingWithResultMap(6);
        System.out.println(student3);
        System.out.println(student3.getHclass());
        System.out.println("=======关联查询返回集合=========");
        ClassMapper classMapper = sqlSession.getMapper(ClassMapper.class);
        Class aClass = classMapper.selectWithTeacher(6);
        System.out.println(aClass);
        System.out.println(aClass.getTeachers());
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void deleteById() throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        TeacherMapper mapper = sqlSession.getMapper(TeacherMapper.class);
        int i=mapper.deleteById(100);
        System.out.println("删除记录数："+i);
        sqlSession.commit();
        sqlSession.close();
        is.close();
    }

    public static void main(String[] args) {
        int[] nums={1,2,3};
        String str="hello";
        StringBuffer sb = new StringBuffer("hello");
        change(nums);
        change(str);
        change(sb);
        int i=10;
        change(i);
        System.out.println(Arrays.toString(nums));//{10,2,3}
        System.out.println(str);//hello
        System.out.println(sb);//hello james
        System.out.println(i);//10

        String str2=new String("hello");
        System.out.println(str==str2);//地址值不等，一个是在常量池的地址，一个是在堆中的地址
        change(str2);
        System.out.println(str2);
    }
    // java中所有的方法参数传递都是值传递。基本数据类型的传递的是字面量值，引用数据类型的传递的是地址值。
    // 字符串是不可变的。
    public static void change(String str){
        str=" james";
    }
    public static void change(StringBuffer sb){
        sb=sb.append(" james");
    }
    public static void change(int i){
        i+=100;
    }
    public static void change(int[] nums){
        nums[0]=10;
    }
}
