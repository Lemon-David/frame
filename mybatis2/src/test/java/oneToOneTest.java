import com.seecen.mybatis2.mapper.ClassMapper;
import com.seecen.mybatis2.mapper.StudentMapper;
import com.seecen.mybatis2.pojo.Class;
import com.seecen.mybatis2.pojo.Student;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;

public class oneToOneTest {
    SqlSession sqlSession;
    SqlSessionFactory sqlSessionFactory;
    @Before//在测试方法执行之前执行
    public void before() throws IOException {
        //1.加载配置文件
        InputStream is = Resources.getResourceAsStream("mybatis.xml");
        //2.创建SqlSessionFactory
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        //3.通过sqlSessionFactory获取SqlSession
        sqlSession = sqlSessionFactory.openSession();
    }

    @Test
    public void oneToOne() {
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        Student student = mapper.selectByIdMappingWithResultMap(2);
        System.out.println(student);
        System.out.println(student.getStudentInfo());
//        BigDecimal// 对数值的精确计算
    }

    @Test
    public void oneToMany() {
        ClassMapper mapper = sqlSession.getMapper(ClassMapper.class);
        Class aClass = mapper.selectWithStudent2(1);
        System.out.println(aClass.getId()+" "+aClass.getName());
        System.out.println(aClass);
//        System.out.println(aClass.getStudents());
    }

    @Test
    public void mapping() {
        ClassMapper mapper = sqlSession.getMapper(ClassMapper.class);
        Class aClass = mapper.selectWithStudentMapping(1);
        System.out.println(aClass);
        System.out.println(aClass.getStudents());
    }

    @Test//测试单个结果映射
    public void mapping2() {
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        Student student = mapper.selectWithClassMapping(1);
        System.out.println(student);
        System.out.println(student.getHclass());
    }

    @Test
    public void selectWithTeacherMapping() {
        ClassMapper mapper = sqlSession.getMapper(ClassMapper.class);
        Class aClass = mapper.selectWithTeacherMapping(6);
        System.out.println(aClass);
        System.out.println(aClass.getTeachers());
    }

    @Test
    public void selectWithStudentInfoMapping() {
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        Student student = mapper.selectWithStudentInfoMapping(2);
        System.out.println(student);
        System.out.println(student.getStudentInfo());
    }

    @Test
    public void cacheTest() {
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
//        List<Student> students = mapper.selectAll();
//        System.out.println(students);
        Student student = mapper.selectById(2);
        System.out.println(student);
        //如果执行了DML操作，则会清空缓存
        student.setStudentName("张无忌");
        mapper.update(student);
        //第二次查询会直接从一级缓存中获取
        Student student2 = mapper.selectById(2);
        System.out.println(student2);
    }
    //测试二级缓存
    @Test
    public void CacheTest2() {
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        Student student = mapper.selectById(2);
        System.out.println(student);
        //sqlSession必须commit才会将数据放入二级缓存
        sqlSession.commit();
        //开启第二个sqlSession
        System.out.println("=============第二个sqlSession查询============");
        SqlSession sqlSession2 = sqlSessionFactory.openSession();
        StudentMapper mapper1 = sqlSession2.getMapper(StudentMapper.class);
        Student student1 = mapper1.selectById(2);
        System.out.println(student1);
        sqlSession2.close();
    }

    @After//在测试方法执行之后执行
    public void after(){
        sqlSession.commit();
        sqlSession.close();
    }
}
