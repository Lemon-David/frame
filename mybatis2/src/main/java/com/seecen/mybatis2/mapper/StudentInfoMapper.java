package com.seecen.mybatis2.mapper;

import com.seecen.mybatis2.pojo.StudentInfo;
import org.apache.ibatis.annotations.Select;

public interface StudentInfoMapper {
    StudentInfo selectById(Integer id);
    StudentInfo selectByIdMappingWithResultMap(Integer id);
    @Select("select id,age as studentInfoAge,address from H_STUDENT_INFO where id=#{id}")
    StudentInfo selectByStudentId(Integer id);
}
