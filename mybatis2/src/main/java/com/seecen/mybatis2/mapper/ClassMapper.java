package com.seecen.mybatis2.mapper;

import com.seecen.mybatis2.pojo.Class;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
//配置二级缓存的注解 替代<cache>
@CacheNamespace()
public interface ClassMapper {
    Class selectById(Integer id);
    List<Class> selectAll();
    int deleteById(Integer id);
    int insert(Class aClass);
    int update(Class aClass);
    List<Class> selectAllList(String orderColumn);
    List<Class> selectByIdOrName(@Param("i") Integer id,@Param("n") String name);
    List<Class> selectByMap(Map<String,Object> map);
    int deleteByIds(Integer[] ids);
    int batchInsert(List<Class> classList);
    int updateSelective(Class aclass);
    int insertSelective(Class aclass);
    List<Class> selectByName(@Param("name") String name);
    /**
     * 查询班级信息并关联查询老师信息的
     * @param id
     * @return
     */
    Class selectWithTeacher(Integer id);

    Class selectWithStudent(Integer id);
    /**
     * 查询班级时关联查询学生信息
     * @param id
     * @return
     */
    Class selectWithStudent2(Integer id);

    /**
     * 通过resultMap映射复杂结果集
     * @param id
     * @return
     */
    Class selectWithStudentMapping(Integer id);
    Class selectWithTeacherMapping(Integer id);
}
