package com.seecen.mybatis2.mapper;

import com.seecen.mybatis2.pojo.Teacher;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 面向接口编程：所有的方法的定义都是通过接口来定义
 */
public interface TeacherMapper {
    /**
     * 根据主键查询
     * @param id
     * @return
     */
    Teacher selectById(Integer id);

    /**
     * 根据名称进行模糊查询
     * @param name
     * @return
     */
    List<Teacher> selectByName(@Param("name") String name);

    /**
     * 根据主键删除
     * @param id
     */
    int deleteById(Integer id);

    /**
     * 新增
     * @param teacher
     * @return 新增记录条数
     */
    int insert(Teacher teacher);

    /**
     * 选择性插入数据
     * @param teacher
     * @return
     */
    int insertSelective(Teacher teacher);
    /**
     * 修改
     * @return teacher
     */
    int update(Teacher teacher);

    /**
     * 选择性更新
     * @param teacher
     * @return
     */
    int updateSelective(Teacher teacher);

    /**
     * 查询所有数据
     * @param oderColumn 指定排序列
     * @return
     */
    List<Teacher> selectAll(String oderColumn);

    /**
     * 根据ID或根据名称查询，演示多个参数的取值方式 通过@Param取别名
     * @param id
     * @param name
     * @return
     */
    List<Teacher> selectByIdOrName(@Param("a") Integer id,@Param("name") String name);

    /**
     * 参数传递的是Map时，#{key} 取value
     * @param map
     * @return
     */
    List<Teacher> selectByMap(Map<String,Object> map);

    /**
     * 批量删除
     * 传递数组
     * @param ids
     * @return
     */
    int deleteByIds(Integer[] ids);

    /**
     * 批量插入
     * @param teacherList
     * @return
     */
    int batchInsert(List<Teacher> teacherList);
    List<Teacher> selectByClassId(Integer classid);
    Teacher selectWithClassMapping(Integer id);
}
