package com.seecen.mybatis2.mapper;

import com.seecen.mybatis2.pojo.Student;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface StudentMapper {
    /**
     * 根据主键查询
     * @param id
     * @return
     */
    Student selectById(Integer id);

    /**
     * 演示通过resultMap进行结果集映射
     * @param id
     * @return
     */
    Student selectByIdMappingWithResultMap(Integer id);

    /**
     * 通过注解方式编写插入
     * statement 编写selectKey的sql
     * @param student
     * @return
     */
    @SelectKey(keyProperty = "id" ,before = true,resultType = Integer.class,
            statement = "select seq_h_student.nextval from dual")
    @Insert("insert into h_student (id,name) values (#{id},#{studentName})")
    int insert(Student student);

    /**
     * 只写@Select 默认是返回值类型为ResultType
     * @Results 指定ResultMap
     * @return
     */
    @Results(id = "baseResultMap",value = {
            @Result(id = true,property = "id",column = "id"),
            @Result(property = "studentName",column = "name"),
            @Result(property = "classid",column = "classid"),
            @Result(property = "version",column = "version")
    })
    @Select("select * from h_student")
    List<Student> selectAll();

    @Update("update h_student set name=#{studentName} where id=#{id}")
    int update(Student student);

    @Delete("delete from h_student where id=#{id}")
    int delete(Integer id);

    Student selectByIdMappingWithStudentInfo(Integer id);
    List<Student> selectByClassIdWithStudent(Integer classId);

    @Select("select id, name as studentName, classid, version from H_STUDENT where classid=#{classid}")
    List<Student> selectByClassid(Integer classid);

    /**
     * 查询学生和班级信息，并通过ResultMap映射到单个Student类中
     * @param id
     * @return
     */
    Student selectWithClassMapping(Integer id);

    /**
     * 查询学生和学生详细信息 并通过resultMap映射
     * @param id
     * @return
     */
    Student selectWithStudentInfoMapping(Integer id);
}
