package com.seecen.mybatis2.pojo;

import java.io.Serializable;

public class StudentInfo implements Serializable {
    private Integer id;
    private Integer studentInfoAge;
    private String address;

//    public void setAge(Integer age){
//        this.studentInfoAge=age;
//    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStudentInfoAge() {
        return studentInfoAge;
    }

    public void setStudentInfoAge(Integer studentInfoAge) {
        this.studentInfoAge = studentInfoAge;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "StudentInfo{" +
                "id=" + id +
                ", studentInfoAge=" + studentInfoAge +
                ", address='" + address + '\'' +
                '}';
    }
}
