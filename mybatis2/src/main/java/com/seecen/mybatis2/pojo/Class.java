package com.seecen.mybatis2.pojo;

import java.io.Serializable;
import java.util.List;
//班级类
public class Class implements Serializable {
    private Integer id;
    private String name;
    private Integer test;
    //多对多
    // 1 hclass 写一个查询（resultMap）
    // 2 在resultMap配置关联查询
    // 3 teacherMapper写一个根据班级ID查询老师信息的方法
    private List<Teacher> teachers;
    private List<Student> students;

    public Class() {
    }

    public Class(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTest() {
        return test;
    }

    public void setTest(Integer test) {
        this.test = test;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Class{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", test=" + test +
                '}';
    }

}
