package com.seecen.mybatis2.pojo;

import java.io.Serializable;

public class Student implements Serializable {
    private Integer id;
    //模拟属性名与列名不一致的情况
    private String studentName;
    private Integer classid;
    //多对一   组合 继承
    private Class hclass;
    private Integer version;
    //
    private StudentInfo studentInfo;

//    public void setName(String name){
//        this.studentName=name;
//    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Integer getClassid() {
        return classid;
    }

    public void setClassid(Integer classid) {
        this.classid = classid;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Class getHclass() {
        return hclass;
    }

    public void setHclass(Class hclass) {
        this.hclass = hclass;
    }

    public StudentInfo getStudentInfo() {
        return studentInfo;
    }

    public void setStudentInfo(StudentInfo studentInfo) {
        this.studentInfo = studentInfo;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", studentName='" + studentName + '\'' +
                ", classid=" + classid +
                ", version=" + version +
                '}';
    }

}
